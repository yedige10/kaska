CKEDITOR.plugins.add( 'FMathEditor', {
    icons: 'FMathEditor',
    init: function( editor ) {
		editor.addCommand( 'viewMathEditor', new CKEDITOR.dialogCommand( 'Dialog' ) );

		editor.ui.addButton( '', {
		    label: 'Math Editor',
		    command: 'viewMathEditor',
		    toolbar: 'insert'
		});

		if ( editor.contextMenu ) {
            editor.addMenuGroup( 'mathGroup' );
            editor.addMenuItem( 'mathEditorItem', {
                label: 'Edit Math',
                icon: this.path + 'icons/.png',
                command: 'viewMathEditor',
                group: 'mathGroup'
            });

            editor.contextMenu.addListener( function( element ) {
                if ( element.getAscendant( 'img', true ) ) {
                    return { mathEditorItem: CKEDITOR.TRISTATE_OFF };
                }
            });
        }

		CKEDITOR.dialog.add( 'Dialog', this.path + 'dialogs/dialog.js' );
    }
});
