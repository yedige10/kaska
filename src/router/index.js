import Vue from 'vue'
import Router from 'vue-router'
import EditorRouter from '@/views/Editor/MainRouter'
import EditorMain from '@/views/Editor/Main'
import EditorQuestionAdding from '@/views/Editor/EditorQuestionAdding'
import Faqs from '@/views/Editor/FAQs'
import Topic from '@/views/Editor/Topic'
import Calendar from '@/views/Editor/Calendar'
import CreateFaq from '@/components/Editor/PageComponents/Editor/FAQ/createFaq'
import ListFaq from '@/components/Editor/PageComponents/Editor/FAQ/listFaq'
import SingleFaq from '@/components/Editor/PageComponents/Editor/FAQ/SingleFaq'
import ListCategoryFaq from '@/components/Editor/PageComponents/Editor/FAQ/ListCategoryFaq'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/editor',
    component: EditorRouter,
    children: [{
        path: '',
        name: 'EditorMain',
        component: EditorMain
      },
      {
        path: 'add-question',
        name: 'EditorQuestionAdding',
        component: EditorQuestionAdding

      },
      {
        path: 'topic',
        name: 'Topic',
        component: Topic
      },
      {
        path: 'calendar',
        name: 'Calendar',
        component: Calendar
      },

      {
        path: 'faqs',

        component: Faqs,
        children: [{
            path: 'listFaq',
            name: 'listFaq',
            component: ListFaq



          },
          {
            path:'listCategoryFaq',
            name:'listCategoryFaq',
            component:ListCategoryFaq
          },
          {
            path: 'createFaq',
            name: 'Create',
            component: CreateFaq
          }, {
            path: 'singleFaq',
            name: 'singleFaq',
            component: SingleFaq
          }
        ]
      }
    ]
  }]
})
