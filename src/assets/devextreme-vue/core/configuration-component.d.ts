/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import { VueConstructor } from "vue";
import Configuration, { ExpectedChild } from "./configuration";
interface IConfigurationOwner {
    $_expectedChildren: Record<string, ExpectedChild>;
}
interface IConfigurationComponent extends IConfigurationOwner {
    $_optionName: string;
    $_isCollectionItem: boolean;
    $_predefinedProps: Record<string, any>;
}
interface IConfigurable extends IConfigurationOwner {
    $_config: Configuration;
}
declare const DxConfiguration: VueConstructor;
export { DxConfiguration, IConfigurable, IConfigurationComponent };
