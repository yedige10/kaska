/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = require("./component");
var DxExtensionComponent = component_1.BaseComponent.extend({
    created: function () {
        this.$_isExtension = true;
    },
    mounted: function () {
        if (this.$vnode && this.$vnode.componentOptions.$_hasOwner) {
            return;
        }
        this.attachTo(this.$el);
    },
    methods: {
        attachTo: function (element) {
            this.$_createWidget(element);
        }
    }
});
exports.DxExtensionComponent = DxExtensionComponent;
