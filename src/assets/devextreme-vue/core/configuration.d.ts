/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import { Vue } from "vue/types/vue";
declare type UpdateFunc = (name: string, value: any) => void;
interface ExpectedChild {
    isCollectionItem: boolean;
    optionName: string;
}
declare class Configuration {
    private readonly _name;
    private readonly _isCollectionItem;
    private readonly _collectionItemIndex;
    private readonly _initialValues;
    private readonly _expectedChildren;
    private readonly _updateFunc;
    private _nestedConfigurations;
    private _optionChangedFunc;
    private _options;
    constructor(updateFunc: UpdateFunc, name: string | null, initialValues: Record<string, any>, expectedChildren?: Record<string, ExpectedChild>, isCollectionItem?: boolean, collectionItemIndex?: number);
    readonly name: string | null;
    readonly fullPath: string | null;
    readonly options: string[];
    readonly initialValues: Record<string, any>;
    readonly expectedChildren: Record<string, ExpectedChild>;
    readonly nested: Configuration[];
    readonly collectionItemIndex: number | undefined;
    readonly isCollectionItem: boolean;
    readonly updateFunc: UpdateFunc;
    init(options: string[]): void;
    optionChangedFunc: any;
    onOptionChanged(args: {
        name: string;
        fullName: string;
        value: any;
    }): void;
    cleanNested(): void;
    createNested(name: string, initialValues: Record<string, any>, isCollectionItem?: boolean, expectedChildren?: Record<string, ExpectedChild>): Configuration;
    updateValue(nestedName: string, value: any): void;
    getInitialValues(): Record<string, any> | undefined;
    getOptionsToWatch(): string[];
}
declare function bindOptionWatchers(config: Configuration, vueInstance: Pick<Vue, "$watch">): void;
declare function subscribeOnUpdates(config: Configuration, vueInstance: Pick<Vue, "$emit">): void;
export default Configuration;
export { bindOptionWatchers, subscribeOnUpdates, UpdateFunc, ExpectedChild };
