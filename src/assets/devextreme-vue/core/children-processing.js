/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
function pullAllChildren(directChildren, allChildren, config) {
    if (!directChildren || directChildren.length === 0) {
        return;
    }
    pullConfigComponents(directChildren, allChildren, config);
}
exports.pullAllChildren = pullAllChildren;
function pullConfigComponents(children, nodes, ownerConfig) {
    children.forEach(function (node) {
        nodes.push(node);
        if (!node.componentOptions) {
            return;
        }
        var configComponent = node.componentOptions.Ctor;
        if (!configComponent.$_optionName) {
            return;
        }
        var initialValues = __assign({}, configComponent.$_predefinedProps, node.componentOptions.propsData);
        var config = ownerConfig.createNested(configComponent.$_optionName, initialValues, configComponent.$_isCollectionItem, configComponent.$_expectedChildren);
        node.componentOptions.$_config = config;
        if (node.componentOptions.children) {
            pullConfigComponents(node.componentOptions.children, nodes, config);
        }
    });
}
