/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import LoadIndicator, { IOptions } from "devextreme/ui/load_indicator";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "elementAttr" | "height" | "hint" | "indicatorSrc" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "rtlEnabled" | "visible" | "width">;
interface DxLoadIndicator extends VueConstructor, AccessibleOptions {
    readonly instance?: LoadIndicator;
}
declare const DxLoadIndicator: DxLoadIndicator;
export default DxLoadIndicator;
export { DxLoadIndicator };
