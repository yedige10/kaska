/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var file_uploader_1 = require("devextreme/ui/file_uploader");
var component_1 = require("./core/component");
var DxFileUploader = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accept: String,
        accessKey: String,
        activeStateEnabled: Boolean,
        allowCanceling: Boolean,
        allowedFileExtensions: Array,
        chunkSize: Number,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        invalidFileExtensionMessage: String,
        invalidMaxFileSizeMessage: String,
        invalidMinFileSizeMessage: String,
        isValid: Boolean,
        labelText: String,
        maxFileSize: Number,
        minFileSize: Number,
        multiple: Boolean,
        name: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onProgress: Function,
        onUploadAborted: Function,
        onUploaded: Function,
        onUploadError: Function,
        onUploadStarted: Function,
        onValueChanged: Function,
        progress: Number,
        readOnly: Boolean,
        readyToUploadMessage: String,
        rtlEnabled: Boolean,
        selectButtonText: String,
        showFileList: Boolean,
        tabIndex: Number,
        uploadButtonText: String,
        uploadedMessage: String,
        uploadFailedMessage: String,
        uploadHeaders: Object,
        uploadMethod: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "POST",
                "PUT"
            ].indexOf(v) !== -1; }
        },
        uploadMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "useButtons",
                "useForm"
            ].indexOf(v) !== -1; }
        },
        uploadUrl: String,
        validationError: Object,
        value: Array,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = file_uploader_1.default;
    }
});
exports.DxFileUploader = DxFileUploader;
exports.default = DxFileUploader;
