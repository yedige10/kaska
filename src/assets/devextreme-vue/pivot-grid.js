/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var pivot_grid_1 = require("devextreme/ui/pivot_grid");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxPivotGrid = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        allowExpandAll: Boolean,
        allowFiltering: Boolean,
        allowSorting: Boolean,
        allowSortingBySummary: Boolean,
        dataFieldArea: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "column",
                "row"
            ].indexOf(v) !== -1; }
        },
        dataSource: [Array, Object],
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        fieldChooser: Object,
        fieldPanel: Object,
        headerFilter: Object,
        height: [Function, Number, String],
        hideEmptySummaryCells: Boolean,
        hint: String,
        loadPanel: Object,
        onCellClick: Function,
        onCellPrepared: Function,
        onContentReady: Function,
        onContextMenuPreparing: Function,
        onDisposing: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rowHeaderLayout: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "standard",
                "tree"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        scrolling: Object,
        showBorders: Boolean,
        showColumnGrandTotals: Boolean,
        showColumnTotals: Boolean,
        showRowGrandTotals: Boolean,
        showRowTotals: Boolean,
        showTotalsPrior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "both",
                "columns",
                "none",
                "rows"
            ].indexOf(v) !== -1; }
        },
        stateStoring: Object,
        tabIndex: Number,
        texts: Object,
        visible: Boolean,
        width: [Function, Number, String],
        wordWrapEnabled: Boolean
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = pivot_grid_1.default;
        this.$_expectedChildren = {
            export: { isCollectionItem: false, optionName: "export" },
            fieldChooser: { isCollectionItem: false, optionName: "fieldChooser" },
            fieldPanel: { isCollectionItem: false, optionName: "fieldPanel" },
            headerFilter: { isCollectionItem: false, optionName: "headerFilter" },
            loadPanel: { isCollectionItem: false, optionName: "loadPanel" },
            pivotGridTexts: { isCollectionItem: false, optionName: "texts" },
            scrolling: { isCollectionItem: false, optionName: "scrolling" },
            stateStoring: { isCollectionItem: false, optionName: "stateStoring" },
            texts: { isCollectionItem: false, optionName: "texts" }
        };
    }
});
exports.DxPivotGrid = DxPivotGrid;
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        fileName: String,
        ignoreExcelErrors: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFieldChooser = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        applyChangesMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "onDemand"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean,
        height: Number,
        layout: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        texts: Object,
        title: String,
        width: Number
    }
});
exports.DxFieldChooser = DxFieldChooser;
DxFieldChooser.$_optionName = "fieldChooser";
DxFieldChooser.$_expectedChildren = {
    fieldChooserTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxFieldChooserTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allFields: String,
        columnFields: String,
        dataFields: String,
        filterFields: String,
        rowFields: String
    }
});
exports.DxFieldChooserTexts = DxFieldChooserTexts;
DxFieldChooserTexts.$_optionName = "texts";
var DxFieldPanel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowFieldDragging: Boolean,
        showColumnFields: Boolean,
        showDataFields: Boolean,
        showFilterFields: Boolean,
        showRowFields: Boolean,
        texts: Object,
        visible: Boolean
    }
});
exports.DxFieldPanel = DxFieldPanel;
DxFieldPanel.$_optionName = "fieldPanel";
DxFieldPanel.$_expectedChildren = {
    fieldPanelTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxFieldPanelTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        columnFieldArea: String,
        dataFieldArea: String,
        filterFieldArea: String,
        rowFieldArea: String
    }
});
exports.DxFieldPanelTexts = DxFieldPanelTexts;
DxFieldPanelTexts.$_optionName = "texts";
var DxHeaderFilter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowSearch: Boolean,
        height: Number,
        searchTimeout: Number,
        texts: Object,
        width: Number
    }
});
exports.DxHeaderFilter = DxHeaderFilter;
DxHeaderFilter.$_optionName = "headerFilter";
DxHeaderFilter.$_expectedChildren = {
    headerFilterTexts: { isCollectionItem: false, optionName: "texts" },
    texts: { isCollectionItem: false, optionName: "texts" }
};
var DxHeaderFilterTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cancel: String,
        emptyValue: String,
        ok: String
    }
});
exports.DxHeaderFilterTexts = DxHeaderFilterTexts;
DxHeaderFilterTexts.$_optionName = "texts";
var DxLoadPanel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        height: Number,
        indicatorSrc: String,
        showIndicator: Boolean,
        showPane: Boolean,
        text: String,
        width: Number
    }
});
exports.DxLoadPanel = DxLoadPanel;
DxLoadPanel.$_optionName = "loadPanel";
var DxPivotGridTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        collapseAll: String,
        dataNotAvailable: String,
        expandAll: String,
        exportToExcel: String,
        grandTotal: String,
        noData: String,
        removeAllSorting: String,
        showFieldChooser: String,
        sortColumnBySummary: String,
        sortRowBySummary: String,
        total: String
    }
});
exports.DxPivotGridTexts = DxPivotGridTexts;
DxPivotGridTexts.$_optionName = "texts";
var DxScrolling = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "standard",
                "virtual"
            ].indexOf(v) !== -1; }
        },
        useNative: Boolean
    }
});
exports.DxScrolling = DxScrolling;
DxScrolling.$_optionName = "scrolling";
var DxStateStoring = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customLoad: Function,
        customSave: Function,
        enabled: Boolean,
        savingTimeout: Number,
        storageKey: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "custom",
                "localStorage",
                "sessionStorage"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStateStoring = DxStateStoring;
DxStateStoring.$_optionName = "stateStoring";
var DxTexts = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allFields: String,
        cancel: String,
        collapseAll: String,
        columnFieldArea: String,
        columnFields: String,
        dataFieldArea: String,
        dataFields: String,
        dataNotAvailable: String,
        emptyValue: String,
        expandAll: String,
        exportToExcel: String,
        filterFieldArea: String,
        filterFields: String,
        grandTotal: String,
        noData: String,
        ok: String,
        removeAllSorting: String,
        rowFieldArea: String,
        rowFields: String,
        showFieldChooser: String,
        sortColumnBySummary: String,
        sortRowBySummary: String,
        total: String
    }
});
exports.DxTexts = DxTexts;
DxTexts.$_optionName = "texts";
exports.default = DxPivotGrid;
