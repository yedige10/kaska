/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Map, { IOptions } from "devextreme/ui/map";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "autoAdjust" | "center" | "controls" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "key" | "markerIconSrc" | "markers" | "onClick" | "onDisposing" | "onInitialized" | "onMarkerAdded" | "onMarkerRemoved" | "onOptionChanged" | "onReady" | "onRouteAdded" | "onRouteRemoved" | "provider" | "routes" | "rtlEnabled" | "tabIndex" | "type" | "visible" | "width" | "zoom">;
interface DxMap extends VueConstructor, AccessibleOptions {
    readonly instance?: Map;
}
declare const DxMap: DxMap;
declare const DxCenter: any;
declare const DxKey: any;
declare const DxLocation: any;
declare const DxMarker: any;
declare const DxRoute: any;
declare const DxTooltip: any;
export default DxMap;
export { DxMap, DxCenter, DxKey, DxLocation, DxMarker, DxRoute, DxTooltip };
