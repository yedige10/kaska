/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TreeList, { IOptions } from "devextreme/ui/tree_list";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "allowColumnReordering" | "allowColumnResizing" | "autoExpandAll" | "cacheEnabled" | "cellHintEnabled" | "columnAutoWidth" | "columnChooser" | "columnFixing" | "columnHidingEnabled" | "columnMinWidth" | "columnResizingMode" | "columns" | "columnWidth" | "customizeColumns" | "dataSource" | "dataStructure" | "dateSerializationFormat" | "disabled" | "editing" | "elementAttr" | "errorRowEnabled" | "expandedRowKeys" | "expandNodesOnFiltering" | "filterBuilder" | "filterBuilderPopup" | "filterPanel" | "filterRow" | "filterSyncEnabled" | "filterValue" | "focusedColumnIndex" | "focusedRowEnabled" | "focusedRowIndex" | "focusedRowKey" | "focusStateEnabled" | "hasItemsExpr" | "headerFilter" | "height" | "highlightChanges" | "hint" | "hoverStateEnabled" | "itemsExpr" | "keyExpr" | "loadPanel" | "noDataText" | "onAdaptiveDetailRowPreparing" | "onCellClick" | "onCellHoverChanged" | "onCellPrepared" | "onContentReady" | "onContextMenuPreparing" | "onDataErrorOccurred" | "onDisposing" | "onEditingStart" | "onEditorPrepared" | "onEditorPreparing" | "onFocusedCellChanged" | "onFocusedCellChanging" | "onFocusedRowChanged" | "onFocusedRowChanging" | "onInitialized" | "onInitNewRow" | "onKeyDown" | "onNodesInitialized" | "onOptionChanged" | "onRowClick" | "onRowCollapsed" | "onRowCollapsing" | "onRowExpanded" | "onRowExpanding" | "onRowInserted" | "onRowInserting" | "onRowPrepared" | "onRowRemoved" | "onRowRemoving" | "onRowUpdated" | "onRowUpdating" | "onRowValidating" | "onSelectionChanged" | "onToolbarPreparing" | "pager" | "paging" | "parentIdExpr" | "remoteOperations" | "renderAsync" | "repaintChangesOnly" | "rootValue" | "rowAlternationEnabled" | "rtlEnabled" | "scrolling" | "searchPanel" | "selectedRowKeys" | "selection" | "showBorders" | "showColumnHeaders" | "showColumnLines" | "showRowLines" | "sorting" | "stateStoring" | "tabIndex" | "twoWayBindingEnabled" | "visible" | "width" | "wordWrapEnabled">;
interface DxTreeList extends VueConstructor, AccessibleOptions {
    readonly instance?: TreeList;
}
declare const DxTreeList: DxTreeList;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxButton: any;
declare const DxButtonItem: any;
declare const DxButtonOptions: any;
declare const DxColCountByScreen: any;
declare const DxCollision: any;
declare const DxColumn: any;
declare const DxColumnChooser: any;
declare const DxColumnFixing: any;
declare const DxColumnFixingTexts: any;
declare const DxColumnHeaderFilter: any;
declare const DxColumnLookup: any;
declare const DxCompareRule: any;
declare const DxCustomOperation: any;
declare const DxCustomRule: any;
declare const DxEditing: any;
declare const DxEditingTexts: any;
declare const DxEmailRule: any;
declare const DxEmptyItem: any;
declare const DxField: any;
declare const DxFieldLookup: any;
declare const DxFilterBuilder: any;
declare const DxFilterBuilderPopup: any;
declare const DxFilterOperationDescriptions: any;
declare const DxFilterPanel: any;
declare const DxFilterPanelTexts: any;
declare const DxFilterRow: any;
declare const DxForm: any;
declare const DxFormat: any;
declare const DxFormItem: any;
declare const DxGroupItem: any;
declare const DxGroupOperationDescriptions: any;
declare const DxHeaderFilter: any;
declare const DxHide: any;
declare const DxItem: any;
declare const DxLabel: any;
declare const DxLoadPanel: any;
declare const DxLookup: any;
declare const DxMy: any;
declare const DxNumericRule: any;
declare const DxOffset: any;
declare const DxOperationDescriptions: any;
declare const DxPager: any;
declare const DxPaging: any;
declare const DxPatternRule: any;
declare const DxPopup: any;
declare const DxPosition: any;
declare const DxRangeRule: any;
declare const DxRemoteOperations: any;
declare const DxRequiredRule: any;
declare const DxScrolling: any;
declare const DxSearchPanel: any;
declare const DxSelection: any;
declare const DxShow: any;
declare const DxSimpleItem: any;
declare const DxSorting: any;
declare const DxStateStoring: any;
declare const DxStringLengthRule: any;
declare const DxTab: any;
declare const DxTabbedItem: any;
declare const DxTabPanelOptions: any;
declare const DxTabPanelOptionsItem: any;
declare const DxTexts: any;
declare const DxToolbarItem: any;
declare const DxTreeListHeaderFilter: any;
declare const DxTreeListHeaderFilterTexts: any;
declare const DxValidationRule: any;
export default DxTreeList;
export { DxTreeList, DxAnimation, DxAt, DxBoundaryOffset, DxButton, DxButtonItem, DxButtonOptions, DxColCountByScreen, DxCollision, DxColumn, DxColumnChooser, DxColumnFixing, DxColumnFixingTexts, DxColumnHeaderFilter, DxColumnLookup, DxCompareRule, DxCustomOperation, DxCustomRule, DxEditing, DxEditingTexts, DxEmailRule, DxEmptyItem, DxField, DxFieldLookup, DxFilterBuilder, DxFilterBuilderPopup, DxFilterOperationDescriptions, DxFilterPanel, DxFilterPanelTexts, DxFilterRow, DxForm, DxFormat, DxFormItem, DxGroupItem, DxGroupOperationDescriptions, DxHeaderFilter, DxHide, DxItem, DxLabel, DxLoadPanel, DxLookup, DxMy, DxNumericRule, DxOffset, DxOperationDescriptions, DxPager, DxPaging, DxPatternRule, DxPopup, DxPosition, DxRangeRule, DxRemoteOperations, DxRequiredRule, DxScrolling, DxSearchPanel, DxSelection, DxShow, DxSimpleItem, DxSorting, DxStateStoring, DxStringLengthRule, DxTab, DxTabbedItem, DxTabPanelOptions, DxTabPanelOptionsItem, DxTexts, DxToolbarItem, DxTreeListHeaderFilter, DxTreeListHeaderFilterTexts, DxValidationRule };
