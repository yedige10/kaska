/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import PivotGridFieldChooser, { IOptions } from "devextreme/ui/pivot_grid_field_chooser";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "allowSearch" | "applyChangesMode" | "dataSource" | "disabled" | "elementAttr" | "focusStateEnabled" | "headerFilter" | "height" | "hint" | "hoverStateEnabled" | "layout" | "onContentReady" | "onContextMenuPreparing" | "onDisposing" | "onInitialized" | "onOptionChanged" | "rtlEnabled" | "searchTimeout" | "state" | "tabIndex" | "texts" | "visible" | "width">;
interface DxPivotGridFieldChooser extends VueConstructor, AccessibleOptions {
    readonly instance?: PivotGridFieldChooser;
}
declare const DxPivotGridFieldChooser: DxPivotGridFieldChooser;
declare const DxHeaderFilter: any;
declare const DxHeaderFilterTexts: any;
declare const DxPivotGridFieldChooserTexts: any;
declare const DxTexts: any;
export default DxPivotGridFieldChooser;
export { DxPivotGridFieldChooser, DxHeaderFilter, DxHeaderFilterTexts, DxPivotGridFieldChooserTexts, DxTexts };
