/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var bar_gauge_1 = require("devextreme/viz/bar_gauge");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxBarGauge = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        animation: Object,
        backgroundColor: String,
        barSpacing: Number,
        baseValue: Number,
        disabled: Boolean,
        elementAttr: Object,
        endValue: Number,
        export: Object,
        geometry: Object,
        label: Object,
        loadingIndicator: Object,
        margin: Object,
        onDisposing: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        pathModified: Boolean,
        redrawOnResize: Boolean,
        relativeInnerRadius: Number,
        rtlEnabled: Boolean,
        size: Object,
        startValue: Number,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        values: Array
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = bar_gauge_1.default;
        this.$_expectedChildren = {
            animation: { isCollectionItem: false, optionName: "animation" },
            export: { isCollectionItem: false, optionName: "export" },
            geometry: { isCollectionItem: false, optionName: "geometry" },
            label: { isCollectionItem: false, optionName: "label" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            size: { isCollectionItem: false, optionName: "size" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" }
        };
    }
});
exports.DxBarGauge = DxBarGauge;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        duration: Number,
        easing: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "easeOutCubic",
                "linear"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGeometry = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endAngle: Number,
        startAngle: Number
    }
});
exports.DxGeometry = DxGeometry;
DxGeometry.$_optionName = "geometry";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        connectorColor: String,
        connectorWidth: Number,
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        indent: Number,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
DxLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" }
};
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" }
};
exports.default = DxBarGauge;
