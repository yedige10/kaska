/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var scheduler_1 = require("devextreme/ui/scheduler");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxScheduler = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        allDayExpr: String,
        appointmentCollectorTemplate: {},
        appointmentTemplate: {},
        appointmentTooltipTemplate: {},
        cellDuration: Number,
        crossScrollingEnabled: Boolean,
        currentDate: {},
        currentView: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "agenda",
                "day",
                "month",
                "timelineDay",
                "timelineMonth",
                "timelineWeek",
                "timelineWorkWeek",
                "week",
                "workWeek"
            ].indexOf(v) !== -1; }
        },
        customizeDateNavigatorText: Function,
        dataCellTemplate: {},
        dataSource: [Array, Object, String],
        dateCellTemplate: {},
        dateSerializationFormat: String,
        descriptionExpr: String,
        disabled: Boolean,
        dropDownAppointmentTemplate: {},
        editing: [Boolean, Object],
        elementAttr: Object,
        endDateExpr: String,
        endDateTimeZoneExpr: String,
        endDayHour: Number,
        firstDayOfWeek: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ].indexOf(v) !== -1; }
        },
        focusStateEnabled: Boolean,
        groupByDate: Boolean,
        groups: Array,
        height: [Function, Number, String],
        hint: String,
        indicatorUpdateInterval: Number,
        max: {},
        maxAppointmentsPerCell: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "unlimited"
            ].indexOf(v) !== -1; }
        },
        min: {},
        noDataText: String,
        onAppointmentAdded: Function,
        onAppointmentAdding: Function,
        onAppointmentClick: [Function, String],
        onAppointmentContextMenu: [Function, String],
        onAppointmentDblClick: [Function, String],
        onAppointmentDeleted: Function,
        onAppointmentDeleting: Function,
        onAppointmentFormOpening: Function,
        onAppointmentRendered: Function,
        onAppointmentUpdated: Function,
        onAppointmentUpdating: Function,
        onCellClick: [Function, String],
        onCellContextMenu: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        recurrenceEditMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dialog",
                "occurrence",
                "series"
            ].indexOf(v) !== -1; }
        },
        recurrenceExceptionExpr: String,
        recurrenceRuleExpr: String,
        remoteFiltering: Boolean,
        resourceCellTemplate: {},
        resources: Array,
        rtlEnabled: Boolean,
        selectedCellData: Array,
        shadeUntilCurrentTime: Boolean,
        showAllDayPanel: Boolean,
        showCurrentTimeIndicator: Boolean,
        startDateExpr: String,
        startDateTimeZoneExpr: String,
        startDayHour: Number,
        tabIndex: Number,
        textExpr: String,
        timeCellTemplate: {},
        timeZone: String,
        useDropDownViewSwitcher: Boolean,
        views: Array,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = scheduler_1.default;
        this.$_expectedChildren = {
            editing: { isCollectionItem: false, optionName: "editing" },
            resource: { isCollectionItem: true, optionName: "resources" },
            view: { isCollectionItem: true, optionName: "views" }
        };
    }
});
exports.DxScheduler = DxScheduler;
var DxEditing = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowAdding: Boolean,
        allowDeleting: Boolean,
        allowDragging: Boolean,
        allowResizing: Boolean,
        allowUpdating: Boolean
    }
});
exports.DxEditing = DxEditing;
DxEditing.$_optionName = "editing";
var DxResource = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowMultiple: Boolean,
        colorExpr: String,
        dataSource: [Array, Object, String],
        displayExpr: [Function, String],
        fieldExpr: String,
        label: String,
        useColorAsDefault: Boolean,
        valueExpr: [Function, String]
    }
});
exports.DxResource = DxResource;
DxResource.$_optionName = "resources";
DxResource.$_isCollectionItem = true;
var DxView = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        agendaDuration: Number,
        appointmentCollectorTemplate: {},
        appointmentTemplate: {},
        appointmentTooltipTemplate: {},
        cellDuration: Number,
        dataCellTemplate: {},
        dateCellTemplate: {},
        dropDownAppointmentTemplate: {},
        endDayHour: Number,
        firstDayOfWeek: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ].indexOf(v) !== -1; }
        },
        groupByDate: Boolean,
        groupOrientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        groups: Array,
        intervalCount: Number,
        maxAppointmentsPerCell: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "unlimited"
            ].indexOf(v) !== -1; }
        },
        name: String,
        resourceCellTemplate: {},
        startDate: {},
        startDayHour: Number,
        timeCellTemplate: {},
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "agenda",
                "day",
                "month",
                "timelineDay",
                "timelineMonth",
                "timelineWeek",
                "timelineWorkWeek",
                "week",
                "workWeek"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxView = DxView;
DxView.$_optionName = "views";
DxView.$_isCollectionItem = true;
exports.default = DxScheduler;
