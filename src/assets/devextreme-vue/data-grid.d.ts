/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import DataGrid, { IOptions } from "devextreme/ui/data_grid";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "allowColumnReordering" | "allowColumnResizing" | "cacheEnabled" | "cellHintEnabled" | "columnAutoWidth" | "columnChooser" | "columnFixing" | "columnHidingEnabled" | "columnMinWidth" | "columnResizingMode" | "columns" | "columnWidth" | "customizeColumns" | "customizeExportData" | "dataSource" | "dateSerializationFormat" | "disabled" | "editing" | "elementAttr" | "errorRowEnabled" | "export" | "filterBuilder" | "filterBuilderPopup" | "filterPanel" | "filterRow" | "filterSyncEnabled" | "filterValue" | "focusedColumnIndex" | "focusedRowEnabled" | "focusedRowIndex" | "focusedRowKey" | "focusStateEnabled" | "grouping" | "groupPanel" | "headerFilter" | "height" | "highlightChanges" | "hint" | "hoverStateEnabled" | "keyExpr" | "loadPanel" | "masterDetail" | "noDataText" | "onAdaptiveDetailRowPreparing" | "onCellClick" | "onCellHoverChanged" | "onCellPrepared" | "onContentReady" | "onContextMenuPreparing" | "onDataErrorOccurred" | "onDisposing" | "onEditingStart" | "onEditorPrepared" | "onEditorPreparing" | "onExported" | "onExporting" | "onFileSaving" | "onFocusedCellChanged" | "onFocusedCellChanging" | "onFocusedRowChanged" | "onFocusedRowChanging" | "onInitialized" | "onInitNewRow" | "onKeyDown" | "onOptionChanged" | "onRowClick" | "onRowCollapsed" | "onRowCollapsing" | "onRowExpanded" | "onRowExpanding" | "onRowInserted" | "onRowInserting" | "onRowPrepared" | "onRowRemoved" | "onRowRemoving" | "onRowUpdated" | "onRowUpdating" | "onRowValidating" | "onSelectionChanged" | "onToolbarPreparing" | "pager" | "paging" | "remoteOperations" | "renderAsync" | "repaintChangesOnly" | "rowAlternationEnabled" | "rowTemplate" | "rtlEnabled" | "scrolling" | "searchPanel" | "selectedRowKeys" | "selection" | "selectionFilter" | "showBorders" | "showColumnHeaders" | "showColumnLines" | "showRowLines" | "sortByGroupSummaryInfo" | "sorting" | "stateStoring" | "summary" | "tabIndex" | "twoWayBindingEnabled" | "visible" | "width" | "wordWrapEnabled">;
interface DxDataGrid extends VueConstructor, AccessibleOptions {
    readonly instance?: DataGrid;
}
declare const DxDataGrid: DxDataGrid;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxButton: any;
declare const DxColCountByScreen: any;
declare const DxCollision: any;
declare const DxColumn: any;
declare const DxColumnChooser: any;
declare const DxColumnFixing: any;
declare const DxColumnFixingTexts: any;
declare const DxColumnHeaderFilter: any;
declare const DxColumnLookup: any;
declare const DxCompareRule: any;
declare const DxCustomOperation: any;
declare const DxCustomRule: any;
declare const DxDataGridHeaderFilter: any;
declare const DxDataGridHeaderFilterTexts: any;
declare const DxEditing: any;
declare const DxEditingTexts: any;
declare const DxEmailRule: any;
declare const DxExport: any;
declare const DxExportTexts: any;
declare const DxField: any;
declare const DxFieldLookup: any;
declare const DxFilterBuilder: any;
declare const DxFilterBuilderPopup: any;
declare const DxFilterOperationDescriptions: any;
declare const DxFilterPanel: any;
declare const DxFilterPanelTexts: any;
declare const DxFilterRow: any;
declare const DxForm: any;
declare const DxFormat: any;
declare const DxFormItem: any;
declare const DxGrouping: any;
declare const DxGroupingTexts: any;
declare const DxGroupItem: any;
declare const DxGroupOperationDescriptions: any;
declare const DxGroupPanel: any;
declare const DxHeaderFilter: any;
declare const DxHide: any;
declare const DxLabel: any;
declare const DxLoadPanel: any;
declare const DxLookup: any;
declare const DxMasterDetail: any;
declare const DxMy: any;
declare const DxNumericRule: any;
declare const DxOffset: any;
declare const DxOperationDescriptions: any;
declare const DxPager: any;
declare const DxPaging: any;
declare const DxPatternRule: any;
declare const DxPopup: any;
declare const DxPosition: any;
declare const DxRangeRule: any;
declare const DxRemoteOperations: any;
declare const DxRequiredRule: any;
declare const DxScrolling: any;
declare const DxSearchPanel: any;
declare const DxSelection: any;
declare const DxShow: any;
declare const DxSortByGroupSummaryInfo: any;
declare const DxSorting: any;
declare const DxStateStoring: any;
declare const DxStringLengthRule: any;
declare const DxSummary: any;
declare const DxSummaryTexts: any;
declare const DxTexts: any;
declare const DxToolbarItem: any;
declare const DxTotalItem: any;
declare const DxValidationRule: any;
declare const DxValueFormat: any;
export default DxDataGrid;
export { DxDataGrid, DxAnimation, DxAt, DxBoundaryOffset, DxButton, DxColCountByScreen, DxCollision, DxColumn, DxColumnChooser, DxColumnFixing, DxColumnFixingTexts, DxColumnHeaderFilter, DxColumnLookup, DxCompareRule, DxCustomOperation, DxCustomRule, DxDataGridHeaderFilter, DxDataGridHeaderFilterTexts, DxEditing, DxEditingTexts, DxEmailRule, DxExport, DxExportTexts, DxField, DxFieldLookup, DxFilterBuilder, DxFilterBuilderPopup, DxFilterOperationDescriptions, DxFilterPanel, DxFilterPanelTexts, DxFilterRow, DxForm, DxFormat, DxFormItem, DxGrouping, DxGroupingTexts, DxGroupItem, DxGroupOperationDescriptions, DxGroupPanel, DxHeaderFilter, DxHide, DxLabel, DxLoadPanel, DxLookup, DxMasterDetail, DxMy, DxNumericRule, DxOffset, DxOperationDescriptions, DxPager, DxPaging, DxPatternRule, DxPopup, DxPosition, DxRangeRule, DxRemoteOperations, DxRequiredRule, DxScrolling, DxSearchPanel, DxSelection, DxShow, DxSortByGroupSummaryInfo, DxSorting, DxStateStoring, DxStringLengthRule, DxSummary, DxSummaryTexts, DxTexts, DxToolbarItem, DxTotalItem, DxValidationRule, DxValueFormat };
