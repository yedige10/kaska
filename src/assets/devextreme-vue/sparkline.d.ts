/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Sparkline, { IOptions } from "devextreme/viz/sparkline";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "argumentField" | "barNegativeColor" | "barPositiveColor" | "dataSource" | "disabled" | "elementAttr" | "firstLastColor" | "ignoreEmptyPoints" | "lineColor" | "lineWidth" | "lossColor" | "margin" | "maxColor" | "maxValue" | "minColor" | "minValue" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onTooltipHidden" | "onTooltipShown" | "pathModified" | "pointColor" | "pointSize" | "pointSymbol" | "rtlEnabled" | "showFirstLast" | "showMinMax" | "size" | "theme" | "tooltip" | "type" | "valueField" | "winColor" | "winlossThreshold">;
interface DxSparkline extends VueConstructor, AccessibleOptions {
    readonly instance?: Sparkline;
}
declare const DxSparkline: DxSparkline;
declare const DxBorder: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxMargin: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxTooltip: any;
export default DxSparkline;
export { DxSparkline, DxBorder, DxFont, DxFormat, DxMargin, DxShadow, DxSize, DxTooltip };
