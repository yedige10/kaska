/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import DropDownBox, { IOptions } from "devextreme/ui/drop_down_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "acceptCustomValue" | "accessKey" | "activeStateEnabled" | "contentTemplate" | "dataSource" | "deferRendering" | "disabled" | "displayExpr" | "dropDownButtonTemplate" | "dropDownOptions" | "elementAttr" | "fieldTemplate" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "items" | "name" | "onChange" | "onClosed" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOpened" | "onOptionChanged" | "onPaste" | "onValueChanged" | "opened" | "openOnFieldClick" | "placeholder" | "readOnly" | "rtlEnabled" | "showClearButton" | "showDropDownButton" | "stylingMode" | "tabIndex" | "text" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "valueExpr" | "visible" | "width">;
interface DxDropDownBox extends VueConstructor, AccessibleOptions {
    readonly instance?: DropDownBox;
}
declare const DxDropDownBox: DxDropDownBox;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxDropDownOptions: any;
declare const DxHide: any;
declare const DxItem: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
declare const DxToolbarItem: any;
export default DxDropDownBox;
export { DxDropDownBox, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxDropDownOptions, DxHide, DxItem, DxMy, DxOffset, DxPosition, DxShow, DxToolbarItem };
