/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var validator_1 = require("devextreme/ui/validator");
var configuration_component_1 = require("./core/configuration-component");
var extension_component_1 = require("./core/extension-component");
var DxValidator = Vue.extend({
    extends: extension_component_1.DxExtensionComponent,
    props: {
        adapter: Object,
        elementAttr: Object,
        height: [Function, Number, String],
        name: String,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValidated: Function,
        validationGroup: String,
        validationRules: Array,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = validator_1.default;
        this.$_expectedChildren = {
            adapter: { isCollectionItem: false, optionName: "adapter" },
            CompareRule: { isCollectionItem: true, optionName: "validationRules" },
            CustomRule: { isCollectionItem: true, optionName: "validationRules" },
            EmailRule: { isCollectionItem: true, optionName: "validationRules" },
            NumericRule: { isCollectionItem: true, optionName: "validationRules" },
            PatternRule: { isCollectionItem: true, optionName: "validationRules" },
            RangeRule: { isCollectionItem: true, optionName: "validationRules" },
            RequiredRule: { isCollectionItem: true, optionName: "validationRules" },
            StringLengthRule: { isCollectionItem: true, optionName: "validationRules" },
            validationRule: { isCollectionItem: true, optionName: "validationRules" }
        };
    }
});
exports.DxValidator = DxValidator;
var DxAdapter = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        applyValidationResults: Function,
        bypass: Function,
        focus: Function,
        getValue: Function,
        reset: Function,
        validationRequestsCallbacks: [Array, Function]
    }
});
exports.DxAdapter = DxAdapter;
DxAdapter.$_optionName = "adapter";
var DxCompareRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCompareRule = DxCompareRule;
DxCompareRule.$_optionName = "validationRules";
DxCompareRule.$_isCollectionItem = true;
DxCompareRule.$_predefinedProps = {
    type: "compare"
};
var DxCustomRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxCustomRule = DxCustomRule;
DxCustomRule.$_optionName = "validationRules";
DxCustomRule.$_isCollectionItem = true;
DxCustomRule.$_predefinedProps = {
    type: "custom"
};
var DxEmailRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxEmailRule = DxEmailRule;
DxEmailRule.$_optionName = "validationRules";
DxEmailRule.$_isCollectionItem = true;
DxEmailRule.$_predefinedProps = {
    type: "email"
};
var DxNumericRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxNumericRule = DxNumericRule;
DxNumericRule.$_optionName = "validationRules";
DxNumericRule.$_isCollectionItem = true;
DxNumericRule.$_predefinedProps = {
    type: "numeric"
};
var DxPatternRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        pattern: {},
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxPatternRule = DxPatternRule;
DxPatternRule.$_optionName = "validationRules";
DxPatternRule.$_isCollectionItem = true;
DxPatternRule.$_predefinedProps = {
    type: "pattern"
};
var DxRangeRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRangeRule = DxRangeRule;
DxRangeRule.$_optionName = "validationRules";
DxRangeRule.$_isCollectionItem = true;
DxRangeRule.$_predefinedProps = {
    type: "range"
};
var DxRequiredRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        message: String,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRequiredRule = DxRequiredRule;
DxRequiredRule.$_optionName = "validationRules";
DxRequiredRule.$_isCollectionItem = true;
DxRequiredRule.$_predefinedProps = {
    type: "required"
};
var DxStringLengthRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: Number,
        message: String,
        min: Number,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStringLengthRule = DxStringLengthRule;
DxStringLengthRule.$_optionName = "validationRules";
DxStringLengthRule.$_isCollectionItem = true;
DxStringLengthRule.$_predefinedProps = {
    type: "stringLength"
};
var DxValidationRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        pattern: {},
        reevaluate: Boolean,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxValidationRule = DxValidationRule;
DxValidationRule.$_optionName = "validationRules";
DxValidationRule.$_isCollectionItem = true;
DxValidationRule.$_predefinedProps = {
    type: "required"
};
exports.default = DxValidator;
