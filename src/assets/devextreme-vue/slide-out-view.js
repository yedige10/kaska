/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var slide_out_view_1 = require("devextreme/ui/slide_out_view");
var component_1 = require("./core/component");
var DxSlideOutView = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        activeStateEnabled: Boolean,
        contentTemplate: {},
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        menuPosition: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "inverted",
                "normal"
            ].indexOf(v) !== -1; }
        },
        menuTemplate: {},
        menuVisible: Boolean,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        swipeEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = slide_out_view_1.default;
    }
});
exports.DxSlideOutView = DxSlideOutView;
exports.default = DxSlideOutView;
