/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Lookup, { IOptions } from "devextreme/ui/lookup";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animation" | "applyButtonText" | "applyValueMode" | "cancelButtonText" | "cleanSearchOnOpening" | "clearButtonText" | "closeOnOutsideClick" | "dataSource" | "deferRendering" | "disabled" | "displayExpr" | "displayValue" | "elementAttr" | "fieldTemplate" | "focusStateEnabled" | "fullScreen" | "grouped" | "groupTemplate" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "items" | "itemTemplate" | "minSearchLength" | "name" | "nextButtonText" | "noDataText" | "onClosed" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onOpened" | "onOptionChanged" | "onPageLoading" | "onPullRefresh" | "onScroll" | "onSelectionChanged" | "onTitleRendered" | "onValueChanged" | "opened" | "pageLoadingText" | "pageLoadMode" | "placeholder" | "popupHeight" | "popupWidth" | "position" | "pulledDownText" | "pullingDownText" | "pullRefreshEnabled" | "refreshingText" | "rtlEnabled" | "searchEnabled" | "searchExpr" | "searchMode" | "searchPlaceholder" | "searchTimeout" | "selectedItem" | "shading" | "showCancelButton" | "showClearButton" | "showDataBeforeSearch" | "showPopupTitle" | "stylingMode" | "tabIndex" | "text" | "title" | "titleTemplate" | "useNativeScrolling" | "usePopover" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "valueExpr" | "visible" | "width">;
interface DxLookup extends VueConstructor, AccessibleOptions {
    readonly instance?: Lookup;
}
declare const DxLookup: DxLookup;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxHide: any;
declare const DxItem: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
export default DxLookup;
export { DxLookup, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxHide, DxItem, DxMy, DxOffset, DxPosition, DxShow };
