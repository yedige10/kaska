/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import VectorMap, { IOptions } from "devextreme/viz/vector_map";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "background" | "bounds" | "center" | "controlBar" | "disabled" | "elementAttr" | "export" | "layers" | "legends" | "loadingIndicator" | "maxZoomFactor" | "onCenterChanged" | "onClick" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onSelectionChanged" | "onTooltipHidden" | "onTooltipShown" | "onZoomFactorChanged" | "panningEnabled" | "pathModified" | "projection" | "redrawOnResize" | "rtlEnabled" | "size" | "theme" | "title" | "tooltip" | "touchEnabled" | "wheelEnabled" | "zoomFactor" | "zoomingEnabled">;
interface DxVectorMap extends VueConstructor, AccessibleOptions {
    readonly instance?: VectorMap;
}
declare const DxVectorMap: DxVectorMap;
declare const DxBackground: any;
declare const DxBorder: any;
declare const DxControlBar: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxLabel: any;
declare const DxLayer: any;
declare const DxLegend: any;
declare const DxLegendBorder: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSource: any;
declare const DxSubtitle: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
export default DxVectorMap;
export { DxVectorMap, DxBackground, DxBorder, DxControlBar, DxExport, DxFont, DxFormat, DxLabel, DxLayer, DxLegend, DxLegendBorder, DxLoadingIndicator, DxMargin, DxShadow, DxSize, DxSource, DxSubtitle, DxTitle, DxTooltip, DxTooltipBorder };
