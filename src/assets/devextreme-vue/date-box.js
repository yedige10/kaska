/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var date_box_1 = require("devextreme/ui/date_box");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxDateBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        acceptCustomValue: Boolean,
        accessKey: String,
        activeStateEnabled: Boolean,
        adaptivityEnabled: Boolean,
        applyButtonText: String,
        applyValueMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "instantly",
                "useButtons"
            ].indexOf(v) !== -1; }
        },
        calendarOptions: Object,
        cancelButtonText: String,
        dateOutOfRangeMessage: String,
        dateSerializationFormat: String,
        deferRendering: Boolean,
        disabled: Boolean,
        disabledDates: [Array, Function],
        displayFormat: [Object, Function, String],
        dropDownButtonTemplate: {},
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        interval: Number,
        invalidDateMessage: String,
        isValid: Boolean,
        max: {},
        maxLength: [Number, String],
        min: {},
        name: String,
        onChange: Function,
        onClosed: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOpened: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        opened: Boolean,
        openOnFieldClick: Boolean,
        pickerType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "calendar",
                "list",
                "native",
                "rollers"
            ].indexOf(v) !== -1; }
        },
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showAnalogClock: Boolean,
        showClearButton: Boolean,
        showDropDownButton: Boolean,
        spellcheck: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "date",
                "datetime",
                "time"
            ].indexOf(v) !== -1; }
        },
        useMaskBehavior: Boolean,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        valueChangeEvent: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = date_box_1.default;
        this.$_expectedChildren = {
            calendarOptions: { isCollectionItem: false, optionName: "calendarOptions" },
            displayFormat: { isCollectionItem: false, optionName: "displayFormat" }
        };
    }
});
exports.DxDateBox = DxDateBox;
var DxCalendarOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        bindingOptions: Object,
        cellTemplate: {},
        currentDate: {},
        dateSerializationFormat: String,
        disabled: Boolean,
        disabledDates: [Array, Function],
        elementAttr: Object,
        firstDayOfWeek: {
            type: Number,
            validator: function (v) { return typeof (v) !== "number" || [
                0,
                1,
                2,
                3,
                4,
                5,
                6
            ].indexOf(v) !== -1; }
        },
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        isValid: Boolean,
        max: {},
        maxZoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        },
        min: {},
        minZoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        },
        name: String,
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showTodayButton: Boolean,
        tabIndex: Number,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: {},
        visible: Boolean,
        width: [Function, Number, String],
        zoomLevel: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "century",
                "decade",
                "month",
                "year"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCalendarOptions = DxCalendarOptions;
DxCalendarOptions.$_optionName = "calendarOptions";
var DxDisplayFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxDisplayFormat = DxDisplayFormat;
DxDisplayFormat.$_optionName = "displayFormat";
exports.default = DxDateBox;
