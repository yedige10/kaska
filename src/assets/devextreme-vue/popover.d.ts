/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Popover, { IOptions } from "devextreme/ui/popover";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "animation" | "closeOnBackButton" | "closeOnOutsideClick" | "container" | "contentTemplate" | "deferRendering" | "disabled" | "elementAttr" | "height" | "hideEvent" | "hint" | "hoverStateEnabled" | "maxHeight" | "maxWidth" | "minHeight" | "minWidth" | "onContentReady" | "onDisposing" | "onHidden" | "onHiding" | "onInitialized" | "onOptionChanged" | "onShowing" | "onShown" | "onTitleRendered" | "position" | "rtlEnabled" | "shading" | "shadingColor" | "showCloseButton" | "showEvent" | "showTitle" | "target" | "title" | "titleTemplate" | "toolbarItems" | "visible" | "width">;
interface DxPopover extends VueConstructor, AccessibleOptions {
    readonly instance?: Popover;
}
declare const DxPopover: DxPopover;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxHide: any;
declare const DxHideEvent: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
declare const DxShowEvent: any;
declare const DxToolbarItem: any;
export default DxPopover;
export { DxPopover, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxHide, DxHideEvent, DxMy, DxOffset, DxPosition, DxShow, DxShowEvent, DxToolbarItem };
