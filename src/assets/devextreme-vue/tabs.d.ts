/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Tabs, { IOptions } from "devextreme/ui/tabs";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "dataSource" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "keyExpr" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "onSelectionChanged" | "repaintChangesOnly" | "rtlEnabled" | "scrollByContent" | "scrollingEnabled" | "selectedIndex" | "selectedItem" | "selectedItemKeys" | "selectedItems" | "selectionMode" | "showNavButtons" | "tabIndex" | "visible" | "width">;
interface DxTabs extends VueConstructor, AccessibleOptions {
    readonly instance?: Tabs;
}
declare const DxTabs: DxTabs;
declare const DxItem: any;
export default DxTabs;
export { DxTabs, DxItem };
