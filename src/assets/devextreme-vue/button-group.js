/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var button_group_1 = require("devextreme/ui/button_group");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxButtonGroup = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        items: Array,
        itemTemplate: {},
        keyExpr: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        rtlEnabled: Boolean,
        selectedItemKeys: Array,
        selectedItems: Array,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "text",
                "outlined",
                "contained"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = button_group_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxButtonGroup = DxButtonGroup;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        hint: String,
        html: String,
        icon: String,
        template: {},
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "back",
                "danger",
                "default",
                "normal",
                "success"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxButtonGroup;
