/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import FileUploader, { IOptions } from "devextreme/ui/file_uploader";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accept" | "accessKey" | "activeStateEnabled" | "allowCanceling" | "allowedFileExtensions" | "chunkSize" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "invalidFileExtensionMessage" | "invalidMaxFileSizeMessage" | "invalidMinFileSizeMessage" | "isValid" | "labelText" | "maxFileSize" | "minFileSize" | "multiple" | "name" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onProgress" | "onUploadAborted" | "onUploaded" | "onUploadError" | "onUploadStarted" | "onValueChanged" | "progress" | "readOnly" | "readyToUploadMessage" | "rtlEnabled" | "selectButtonText" | "showFileList" | "tabIndex" | "uploadButtonText" | "uploadedMessage" | "uploadFailedMessage" | "uploadHeaders" | "uploadMethod" | "uploadMode" | "uploadUrl" | "validationError" | "value" | "visible" | "width">;
interface DxFileUploader extends VueConstructor, AccessibleOptions {
    readonly instance?: FileUploader;
}
declare const DxFileUploader: DxFileUploader;
export default DxFileUploader;
export { DxFileUploader };
