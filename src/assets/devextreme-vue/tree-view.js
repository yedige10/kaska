/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var tree_view_1 = require("devextreme/ui/tree_view");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxTreeView = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animationEnabled: Boolean,
        createChildren: Function,
        dataSource: [Array, Object, String],
        dataStructure: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "plain",
                "tree"
            ].indexOf(v) !== -1; }
        },
        disabled: Boolean,
        disabledExpr: [Function, String],
        displayExpr: [Function, String],
        elementAttr: Object,
        expandAllEnabled: Boolean,
        expandedExpr: [Function, String],
        expandEvent: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dblclick",
                "click"
            ].indexOf(v) !== -1; }
        },
        expandNodesRecursive: Boolean,
        focusStateEnabled: Boolean,
        hasItemsExpr: [Function, String],
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemsExpr: [Function, String],
        itemTemplate: {},
        keyExpr: [Function, String],
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: Function,
        onItemCollapsed: Function,
        onItemContextMenu: Function,
        onItemExpanded: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onItemSelectionChanged: Function,
        onOptionChanged: Function,
        onSelectAllValueChanged: Function,
        onSelectionChanged: Function,
        parentIdExpr: [Function, String],
        rootValue: Object,
        rtlEnabled: Boolean,
        scrollDirection: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "both",
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        searchEditorOptions: Object,
        searchEnabled: Boolean,
        searchExpr: [Array, Function, String],
        searchMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "contains",
                "startswith",
                "equals"
            ].indexOf(v) !== -1; }
        },
        searchTimeout: Number,
        searchValue: String,
        selectAllText: String,
        selectByClick: Boolean,
        selectedExpr: [Function, String],
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single"
            ].indexOf(v) !== -1; }
        },
        selectNodesRecursive: Boolean,
        showCheckBoxesMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "normal",
                "selectAll"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        virtualModeEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = tree_view_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" },
            searchEditorOptions: { isCollectionItem: false, optionName: "searchEditorOptions" }
        };
    }
});
exports.DxTreeView = DxTreeView;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        expanded: Boolean,
        hasItems: Boolean,
        html: String,
        icon: String,
        items: Array,
        parentId: [Number, String],
        selected: Boolean,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxSearchEditorOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        bindingOptions: Object,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        mask: String,
        maskChar: String,
        maskInvalidMessage: String,
        maskRules: Object,
        maxLength: [Number, String],
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "email",
                "password",
                "search",
                "tel",
                "text",
                "url"
            ].indexOf(v) !== -1; }
        },
        name: String,
        onChange: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showClearButton: Boolean,
        showMaskMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "onFocus"
            ].indexOf(v) !== -1; }
        },
        spellcheck: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        useMaskedValue: Boolean,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: String,
        valueChangeEvent: String,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxSearchEditorOptions = DxSearchEditorOptions;
DxSearchEditorOptions.$_optionName = "searchEditorOptions";
exports.default = DxTreeView;
