/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Funnel, { IOptions } from "devextreme/viz/funnel";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adaptiveLayout" | "algorithm" | "argumentField" | "colorField" | "dataSource" | "disabled" | "elementAttr" | "export" | "hoverEnabled" | "inverted" | "item" | "label" | "legend" | "loadingIndicator" | "margin" | "neckHeight" | "neckWidth" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onHoverChanged" | "onIncidentOccurred" | "onInitialized" | "onItemClick" | "onLegendClick" | "onOptionChanged" | "onSelectionChanged" | "palette" | "paletteExtensionMode" | "pathModified" | "redrawOnResize" | "rtlEnabled" | "selectionMode" | "size" | "sortData" | "theme" | "title" | "tooltip" | "valueField">;
interface DxFunnel extends VueConstructor, AccessibleOptions {
    readonly instance?: Funnel;
}
declare const DxFunnel: DxFunnel;
declare const DxAdaptiveLayout: any;
declare const DxBorder: any;
declare const DxConnector: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxHatching: any;
declare const DxHoverStyle: any;
declare const DxItem: any;
declare const DxItemBorder: any;
declare const DxLabel: any;
declare const DxLabelBorder: any;
declare const DxLegend: any;
declare const DxLegendBorder: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxSelectionStyle: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSubtitle: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
export default DxFunnel;
export { DxFunnel, DxAdaptiveLayout, DxBorder, DxConnector, DxExport, DxFont, DxFormat, DxHatching, DxHoverStyle, DxItem, DxItemBorder, DxLabel, DxLabelBorder, DxLegend, DxLegendBorder, DxLoadingIndicator, DxMargin, DxSelectionStyle, DxShadow, DxSize, DxSubtitle, DxTitle, DxTooltip, DxTooltipBorder };
