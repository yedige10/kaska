/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import DeferRendering, { IOptions } from "devextreme/ui/defer_rendering";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animation" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onRendered" | "onShown" | "renderWhen" | "rtlEnabled" | "showLoadIndicator" | "staggerItemSelector" | "tabIndex" | "visible" | "width">;
interface DxDeferRendering extends VueConstructor, AccessibleOptions {
    readonly instance?: DeferRendering;
}
declare const DxDeferRendering: DxDeferRendering;
declare const DxAnimation: any;
export default DxDeferRendering;
export { DxDeferRendering, DxAnimation };
