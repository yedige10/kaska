/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import FilterBuilder, { IOptions } from "devextreme/ui/filter_builder";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "allowHierarchicalFields" | "customOperations" | "disabled" | "elementAttr" | "fields" | "filterOperationDescriptions" | "focusStateEnabled" | "groupOperationDescriptions" | "groupOperations" | "height" | "hint" | "hoverStateEnabled" | "maxGroupLevel" | "onContentReady" | "onDisposing" | "onEditorPrepared" | "onEditorPreparing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "rtlEnabled" | "tabIndex" | "value" | "visible" | "width">;
interface DxFilterBuilder extends VueConstructor, AccessibleOptions {
    readonly instance?: FilterBuilder;
}
declare const DxFilterBuilder: DxFilterBuilder;
declare const DxCustomOperation: any;
declare const DxField: any;
declare const DxFilterOperationDescriptions: any;
declare const DxFormat: any;
declare const DxGroupOperationDescriptions: any;
declare const DxLookup: any;
export default DxFilterBuilder;
export { DxFilterBuilder, DxCustomOperation, DxField, DxFilterOperationDescriptions, DxFormat, DxGroupOperationDescriptions, DxLookup };
