/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Bullet, { IOptions } from "devextreme/viz/bullet";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "color" | "disabled" | "elementAttr" | "endScaleValue" | "margin" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onTooltipHidden" | "onTooltipShown" | "pathModified" | "rtlEnabled" | "showTarget" | "showZeroLevel" | "size" | "startScaleValue" | "target" | "targetColor" | "targetWidth" | "theme" | "tooltip" | "value">;
interface DxBullet extends VueConstructor, AccessibleOptions {
    readonly instance?: Bullet;
}
declare const DxBullet: DxBullet;
declare const DxBorder: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxMargin: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxTooltip: any;
export default DxBullet;
export { DxBullet, DxBorder, DxFont, DxFormat, DxMargin, DxShadow, DxSize, DxTooltip };
