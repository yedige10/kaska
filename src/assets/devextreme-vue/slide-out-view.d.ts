/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import SlideOutView, { IOptions } from "devextreme/ui/slide_out_view";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "activeStateEnabled" | "contentTemplate" | "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "menuPosition" | "menuTemplate" | "menuVisible" | "onDisposing" | "onInitialized" | "onOptionChanged" | "rtlEnabled" | "swipeEnabled" | "visible" | "width">;
interface DxSlideOutView extends VueConstructor, AccessibleOptions {
    readonly instance?: SlideOutView;
}
declare const DxSlideOutView: DxSlideOutView;
export default DxSlideOutView;
export { DxSlideOutView };
