/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TreeMap, { IOptions } from "devextreme/viz/tree_map";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "childrenField" | "colorField" | "colorizer" | "dataSource" | "disabled" | "elementAttr" | "export" | "group" | "hoverEnabled" | "idField" | "interactWithGroup" | "labelField" | "layoutAlgorithm" | "layoutDirection" | "loadingIndicator" | "maxDepth" | "onClick" | "onDisposing" | "onDrawn" | "onDrill" | "onExported" | "onExporting" | "onFileSaving" | "onHoverChanged" | "onIncidentOccurred" | "onInitialized" | "onNodesInitialized" | "onNodesRendering" | "onOptionChanged" | "onSelectionChanged" | "parentField" | "pathModified" | "redrawOnResize" | "resolveLabelOverflow" | "rtlEnabled" | "selectionMode" | "size" | "theme" | "tile" | "title" | "tooltip" | "valueField">;
interface DxTreeMap extends VueConstructor, AccessibleOptions {
    readonly instance?: TreeMap;
}
declare const DxTreeMap: DxTreeMap;
declare const DxBorder: any;
declare const DxColorizer: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxGroup: any;
declare const DxHoverStyle: any;
declare const DxLabel: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxSelectionStyle: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSubtitle: any;
declare const DxTile: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
declare const DxTreeMapborder: any;
export default DxTreeMap;
export { DxTreeMap, DxBorder, DxColorizer, DxExport, DxFont, DxFormat, DxGroup, DxHoverStyle, DxLabel, DxLoadingIndicator, DxMargin, DxSelectionStyle, DxShadow, DxSize, DxSubtitle, DxTile, DxTitle, DxTooltip, DxTooltipBorder, DxTreeMapborder };
