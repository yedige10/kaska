/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var tree_map_1 = require("devextreme/viz/tree_map");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxTreeMap = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        childrenField: String,
        colorField: String,
        colorizer: Object,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        group: Object,
        hoverEnabled: Boolean,
        idField: String,
        interactWithGroup: Boolean,
        labelField: String,
        layoutAlgorithm: {
            type: [Function, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "sliceanddice",
                "squarified",
                "strip"
            ].indexOf(v) !== -1; }
        },
        layoutDirection: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "leftBottomRightTop",
                "leftTopRightBottom",
                "rightBottomLeftTop",
                "rightTopLeftBottom"
            ].indexOf(v) !== -1; }
        },
        loadingIndicator: Object,
        maxDepth: Number,
        onClick: [Function, String],
        onDisposing: Function,
        onDrawn: Function,
        onDrill: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onHoverChanged: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onNodesInitialized: Function,
        onNodesRendering: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        parentField: String,
        pathModified: Boolean,
        redrawOnResize: Boolean,
        resolveLabelOverflow: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "ellipsis",
                "hide"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        size: Object,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        tile: Object,
        title: [Object, String],
        tooltip: Object,
        valueField: String
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = tree_map_1.default;
        this.$_expectedChildren = {
            colorizer: { isCollectionItem: false, optionName: "colorizer" },
            export: { isCollectionItem: false, optionName: "export" },
            group: { isCollectionItem: false, optionName: "group" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            size: { isCollectionItem: false, optionName: "size" },
            tile: { isCollectionItem: false, optionName: "tile" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" }
        };
    }
});
exports.DxTreeMap = DxTreeMap;
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxColorizer = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colorCodeField: String,
        colorizeGroups: Boolean,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        range: Array,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "discrete",
                "gradient",
                "none",
                "range"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxColorizer = DxColorizer;
DxColorizer.$_optionName = "colorizer";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGroup = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        headerHeight: Number,
        hoverEnabled: Boolean,
        hoverStyle: Object,
        label: Object,
        selectionStyle: Object
    }
});
exports.DxGroup = DxGroup;
DxGroup.$_optionName = "group";
DxGroup.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    treeMapborder: { isCollectionItem: false, optionName: "border" }
};
var DxHoverStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String
    }
});
exports.DxHoverStyle = DxHoverStyle;
DxHoverStyle.$_optionName = "hoverStyle";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxSelectionStyle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String
    }
});
exports.DxSelectionStyle = DxSelectionStyle;
DxSelectionStyle.$_optionName = "selectionStyle";
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxTile = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        border: Object,
        color: String,
        hoverStyle: Object,
        label: Object,
        selectionStyle: Object
    }
});
exports.DxTile = DxTile;
DxTile.$_optionName = "tile";
DxTile.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    hoverStyle: { isCollectionItem: false, optionName: "hoverStyle" },
    label: { isCollectionItem: false, optionName: "label" },
    selectionStyle: { isCollectionItem: false, optionName: "selectionStyle" },
    treeMapborder: { isCollectionItem: false, optionName: "border" }
};
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" },
    tooltipBorder: { isCollectionItem: false, optionName: "border" }
};
var DxTooltipBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTooltipBorder = DxTooltipBorder;
DxTooltipBorder.$_optionName = "border";
var DxTreeMapborder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        width: Number
    }
});
exports.DxTreeMapborder = DxTreeMapborder;
DxTreeMapborder.$_optionName = "border";
exports.default = DxTreeMap;
