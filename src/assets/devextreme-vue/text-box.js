/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var text_box_1 = require("devextreme/ui/text_box");
var component_1 = require("./core/component");
var DxTextBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        inputAttr: Object,
        isValid: Boolean,
        mask: String,
        maskChar: String,
        maskInvalidMessage: String,
        maskRules: Object,
        maxLength: [Number, String],
        mode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "email",
                "password",
                "search",
                "tel",
                "text",
                "url"
            ].indexOf(v) !== -1; }
        },
        name: String,
        onChange: Function,
        onContentReady: Function,
        onCopy: Function,
        onCut: Function,
        onDisposing: Function,
        onEnterKey: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onInput: Function,
        onKeyDown: Function,
        onKeyPress: Function,
        onKeyUp: Function,
        onOptionChanged: Function,
        onPaste: Function,
        onValueChanged: Function,
        placeholder: String,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showClearButton: Boolean,
        showMaskMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "onFocus"
            ].indexOf(v) !== -1; }
        },
        spellcheck: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "outlined",
                "underlined",
                "filled"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        text: String,
        useMaskedValue: Boolean,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: String,
        valueChangeEvent: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = text_box_1.default;
    }
});
exports.DxTextBox = DxTextBox;
exports.default = DxTextBox;
