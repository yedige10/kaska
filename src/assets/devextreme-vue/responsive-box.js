/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var responsive_box_1 = require("devextreme/ui/responsive_box");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxResponsiveBox = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        cols: Array,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        rows: Array,
        rtlEnabled: Boolean,
        screenByWidth: Function,
        singleColumnScreen: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = responsive_box_1.default;
        this.$_expectedChildren = {
            col: { isCollectionItem: true, optionName: "cols" },
            item: { isCollectionItem: true, optionName: "items" },
            row: { isCollectionItem: true, optionName: "rows" }
        };
    }
});
exports.DxResponsiveBox = DxResponsiveBox;
var DxCol = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        baseSize: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto"
            ].indexOf(v) !== -1; }
        },
        ratio: Number,
        screen: String,
        shrink: Number
    }
});
exports.DxCol = DxCol;
DxCol.$_optionName = "cols";
DxCol.$_isCollectionItem = true;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        location: [Array, Object],
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
DxItem.$_expectedChildren = {
    location: { isCollectionItem: true, optionName: "location" }
};
var DxLocation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        col: Number,
        colspan: Number,
        row: Number,
        rowspan: Number,
        screen: String
    }
});
exports.DxLocation = DxLocation;
DxLocation.$_optionName = "location";
DxLocation.$_isCollectionItem = true;
var DxRow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        baseSize: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto"
            ].indexOf(v) !== -1; }
        },
        ratio: Number,
        screen: String,
        shrink: Number
    }
});
exports.DxRow = DxRow;
DxRow.$_optionName = "rows";
DxRow.$_isCollectionItem = true;
exports.default = DxResponsiveBox;
