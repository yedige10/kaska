/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Accordion, { IOptions } from "devextreme/ui/accordion";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animationDuration" | "collapsible" | "dataSource" | "deferRendering" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "itemTitleTemplate" | "keyExpr" | "multiple" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onItemTitleClick" | "onOptionChanged" | "onSelectionChanged" | "repaintChangesOnly" | "rtlEnabled" | "selectedIndex" | "selectedItem" | "selectedItemKeys" | "selectedItems" | "tabIndex" | "visible" | "width">;
interface DxAccordion extends VueConstructor, AccessibleOptions {
    readonly instance?: Accordion;
}
declare const DxAccordion: DxAccordion;
declare const DxItem: any;
export default DxAccordion;
export { DxAccordion, DxItem };
