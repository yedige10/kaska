/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ContextMenu, { IOptions } from "devextreme/ui/context_menu";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "animation" | "closeOnOutsideClick" | "cssClass" | "dataSource" | "disabled" | "disabledExpr" | "displayExpr" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "items" | "itemsExpr" | "itemTemplate" | "onContentReady" | "onDisposing" | "onHidden" | "onHiding" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemRendered" | "onOptionChanged" | "onPositioning" | "onSelectionChanged" | "onShowing" | "onShown" | "position" | "rtlEnabled" | "selectByClick" | "selectedExpr" | "selectedItem" | "selectionMode" | "showEvent" | "showSubmenuMode" | "submenuDirection" | "tabIndex" | "target" | "visible" | "width">;
interface DxContextMenu extends VueConstructor, AccessibleOptions {
    readonly instance?: ContextMenu;
}
declare const DxContextMenu: DxContextMenu;
declare const DxAnimation: any;
declare const DxAt: any;
declare const DxBoundaryOffset: any;
declare const DxCollision: any;
declare const DxDelay: any;
declare const DxHide: any;
declare const DxItem: any;
declare const DxMy: any;
declare const DxOffset: any;
declare const DxPosition: any;
declare const DxShow: any;
declare const DxShowEvent: any;
declare const DxShowSubmenuMode: any;
export default DxContextMenu;
export { DxContextMenu, DxAnimation, DxAt, DxBoundaryOffset, DxCollision, DxDelay, DxHide, DxItem, DxMy, DxOffset, DxPosition, DxShow, DxShowEvent, DxShowSubmenuMode };
