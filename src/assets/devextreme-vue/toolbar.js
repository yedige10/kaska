/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var toolbar_1 = require("devextreme/ui/toolbar");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxToolbar = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        menuItemTemplate: {},
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        renderAs: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottomToolbar",
                "topToolbar"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = toolbar_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxToolbar = DxToolbar;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        cssClass: String,
        disabled: Boolean,
        html: String,
        locateInMenu: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto",
                "never"
            ].indexOf(v) !== -1; }
        },
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "after",
                "before",
                "center"
            ].indexOf(v) !== -1; }
        },
        menuItemTemplate: {},
        options: Object,
        showText: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "inMenu"
            ].indexOf(v) !== -1; }
        },
        template: {},
        text: String,
        visible: Boolean,
        widget: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxButton",
                "dxCheckBox",
                "dxDateBox",
                "dxMenu",
                "dxSelectBox",
                "dxTabs",
                "dxTextBox",
                "dxButtonGroup"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxToolbar;
