/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var circular_gauge_1 = require("devextreme/viz/circular_gauge");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxCircularGauge = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        animation: Object,
        containerBackgroundColor: String,
        disabled: Boolean,
        elementAttr: Object,
        export: Object,
        geometry: Object,
        loadingIndicator: Object,
        margin: Object,
        onDisposing: Function,
        onDrawn: Function,
        onExported: Function,
        onExporting: Function,
        onFileSaving: Function,
        onIncidentOccurred: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onTooltipHidden: Function,
        onTooltipShown: Function,
        pathModified: Boolean,
        rangeContainer: Object,
        redrawOnResize: Boolean,
        rtlEnabled: Boolean,
        scale: Object,
        size: Object,
        subvalueIndicator: Object,
        subvalues: Array,
        theme: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "android5.light",
                "generic.dark",
                "generic.light",
                "generic.contrast",
                "ios7.default",
                "win10.black",
                "win10.white",
                "win8.black",
                "win8.white",
                "generic.carmine",
                "generic.darkmoon",
                "generic.darkviolet",
                "generic.greenmist",
                "generic.softblue",
                "material.blue.light",
                "material.lime.light",
                "material.orange.light",
                "material.purple.light",
                "material.teal.light"
            ].indexOf(v) !== -1; }
        },
        title: [Object, String],
        tooltip: Object,
        value: Number,
        valueIndicator: Object
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = circular_gauge_1.default;
        this.$_expectedChildren = {
            animation: { isCollectionItem: false, optionName: "animation" },
            export: { isCollectionItem: false, optionName: "export" },
            geometry: { isCollectionItem: false, optionName: "geometry" },
            loadingIndicator: { isCollectionItem: false, optionName: "loadingIndicator" },
            margin: { isCollectionItem: false, optionName: "margin" },
            rangeContainer: { isCollectionItem: false, optionName: "rangeContainer" },
            scale: { isCollectionItem: false, optionName: "scale" },
            size: { isCollectionItem: false, optionName: "size" },
            subvalueIndicator: { isCollectionItem: false, optionName: "subvalueIndicator" },
            title: { isCollectionItem: false, optionName: "title" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" },
            valueIndicator: { isCollectionItem: false, optionName: "valueIndicator" }
        };
    }
});
exports.DxCircularGauge = DxCircularGauge;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        duration: Number,
        easing: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "easeOutCubic",
                "linear"
            ].indexOf(v) !== -1; }
        },
        enabled: Boolean
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
var DxBorder = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        dashStyle: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dash",
                "dot",
                "longDash",
                "solid"
            ].indexOf(v) !== -1; }
        },
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxBorder = DxBorder;
DxBorder.$_optionName = "border";
var DxExport = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        enabled: Boolean,
        fileName: String,
        formats: Array,
        margin: Number,
        printingEnabled: Boolean,
        proxyUrl: String
    }
});
exports.DxExport = DxExport;
DxExport.$_optionName = "export";
var DxFont = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        family: String,
        opacity: Number,
        size: [Number, String],
        weight: Number
    }
});
exports.DxFont = DxFont;
DxFont.$_optionName = "font";
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxGeometry = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        endAngle: Number,
        startAngle: Number
    }
});
exports.DxGeometry = DxGeometry;
DxGeometry.$_optionName = "geometry";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        hideFirstOrLast: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "first",
                "last"
            ].indexOf(v) !== -1; }
        },
        indentFromTick: Number,
        overlappingBehavior: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "hide",
                "none"
            ].indexOf(v) !== -1; }
        },
        useRangeColors: Boolean,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
DxLabel.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" }
};
var DxLoadingIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        font: Object,
        show: Boolean,
        text: String
    }
});
exports.DxLoadingIndicator = DxLoadingIndicator;
DxLoadingIndicator.$_optionName = "loadingIndicator";
DxLoadingIndicator.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxMargin = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        bottom: Number,
        left: Number,
        right: Number,
        top: Number
    }
});
exports.DxMargin = DxMargin;
DxMargin.$_optionName = "margin";
var DxMinorTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxMinorTick = DxMinorTick;
DxMinorTick.$_optionName = "minorTick";
var DxRange = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        endValue: Number,
        startValue: Number
    }
});
exports.DxRange = DxRange;
DxRange.$_optionName = "ranges";
DxRange.$_isCollectionItem = true;
var DxRangeContainer = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        backgroundColor: String,
        offset: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        paletteExtensionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "alternate",
                "blend",
                "extrapolate"
            ].indexOf(v) !== -1; }
        },
        ranges: Array,
        width: Number
    }
});
exports.DxRangeContainer = DxRangeContainer;
DxRangeContainer.$_optionName = "rangeContainer";
DxRangeContainer.$_expectedChildren = {
    range: { isCollectionItem: true, optionName: "ranges" }
};
var DxScale = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        allowDecimals: Boolean,
        customMinorTicks: Array,
        customTicks: Array,
        endValue: Number,
        label: Object,
        minorTick: Object,
        minorTickInterval: Number,
        orientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "inside",
                "outside"
            ].indexOf(v) !== -1; }
        },
        startValue: Number,
        tick: Object,
        tickInterval: Number
    }
});
exports.DxScale = DxScale;
DxScale.$_optionName = "scale";
DxScale.$_expectedChildren = {
    label: { isCollectionItem: false, optionName: "label" },
    minorTick: { isCollectionItem: false, optionName: "minorTick" },
    tick: { isCollectionItem: false, optionName: "tick" }
};
var DxShadow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        blur: Number,
        color: String,
        offsetX: Number,
        offsetY: Number,
        opacity: Number
    }
});
exports.DxShadow = DxShadow;
DxShadow.$_optionName = "shadow";
var DxSize = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        height: Number,
        width: Number
    }
});
exports.DxSize = DxSize;
DxSize.$_optionName = "size";
var DxSubtitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        text: String
    }
});
exports.DxSubtitle = DxSubtitle;
DxSubtitle.$_optionName = "subtitle";
DxSubtitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" }
};
var DxSubvalueIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        backgroundColor: String,
        baseValue: Number,
        beginAdaptingAtRadius: Number,
        color: String,
        horizontalOrientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        indentFromCenter: Number,
        length: Number,
        offset: Number,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        secondColor: String,
        secondFraction: Number,
        size: Number,
        spindleGapSize: Number,
        spindleSize: Number,
        text: Object,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "rangeBar",
                "rectangle",
                "rectangleNeedle",
                "rhombus",
                "textCloud",
                "triangleMarker",
                "triangleNeedle",
                "twoColorNeedle"
            ].indexOf(v) !== -1; }
        },
        verticalOrientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        width: Number
    }
});
exports.DxSubvalueIndicator = DxSubvalueIndicator;
DxSubvalueIndicator.$_optionName = "subvalueIndicator";
var DxText = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        customizeText: Function,
        font: Object,
        format: [Object, Function, String],
        indent: Number
    }
});
exports.DxText = DxText;
DxText.$_optionName = "text";
DxText.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" }
};
var DxTick = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        color: String,
        length: Number,
        opacity: Number,
        visible: Boolean,
        width: Number
    }
});
exports.DxTick = DxTick;
DxTick.$_optionName = "tick";
var DxTitle = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        font: Object,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        margin: [Number, Object],
        placeholderSize: Number,
        subtitle: [Object, String],
        text: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTitle = DxTitle;
DxTitle.$_optionName = "title";
DxTitle.$_expectedChildren = {
    font: { isCollectionItem: false, optionName: "font" },
    margin: { isCollectionItem: false, optionName: "margin" },
    subtitle: { isCollectionItem: false, optionName: "subtitle" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        border: Object,
        color: String,
        container: {},
        customizeTooltip: Function,
        enabled: Boolean,
        font: Object,
        format: [Object, Function, String],
        opacity: Number,
        paddingLeftRight: Number,
        paddingTopBottom: Number,
        shadow: Object,
        zIndex: Number
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    border: { isCollectionItem: false, optionName: "border" },
    font: { isCollectionItem: false, optionName: "font" },
    format: { isCollectionItem: false, optionName: "format" },
    shadow: { isCollectionItem: false, optionName: "shadow" }
};
var DxValueIndicator = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        arrowLength: Number,
        backgroundColor: String,
        baseValue: Number,
        beginAdaptingAtRadius: Number,
        color: String,
        horizontalOrientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        indentFromCenter: Number,
        length: Number,
        offset: Number,
        palette: {
            type: [Array, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "Bright",
                "Default",
                "Harmony Light",
                "Ocean",
                "Pastel",
                "Soft",
                "Soft Pastel",
                "Vintage",
                "Violet",
                "Carmine",
                "Dark Moon",
                "Dark Violet",
                "Green Mist",
                "Soft Blue",
                "Material",
                "Office"
            ].indexOf(v) !== -1; }
        },
        secondColor: String,
        secondFraction: Number,
        size: Number,
        spindleGapSize: Number,
        spindleSize: Number,
        text: Object,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "circle",
                "rangeBar",
                "rectangle",
                "rectangleNeedle",
                "rhombus",
                "textCloud",
                "triangleMarker",
                "triangleNeedle",
                "twoColorNeedle"
            ].indexOf(v) !== -1; }
        },
        verticalOrientation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        width: Number
    }
});
exports.DxValueIndicator = DxValueIndicator;
DxValueIndicator.$_optionName = "valueIndicator";
DxValueIndicator.$_expectedChildren = {
    text: { isCollectionItem: false, optionName: "text" }
};
exports.default = DxCircularGauge;
