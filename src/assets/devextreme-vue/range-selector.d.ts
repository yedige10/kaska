/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import RangeSelector, { IOptions } from "devextreme/viz/range_selector";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "background" | "behavior" | "chart" | "containerBackgroundColor" | "dataSource" | "dataSourceField" | "disabled" | "elementAttr" | "export" | "indent" | "loadingIndicator" | "margin" | "onDisposing" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "pathModified" | "redrawOnResize" | "rtlEnabled" | "scale" | "selectedRangeColor" | "selectedRangeUpdateMode" | "shutter" | "size" | "sliderHandle" | "sliderMarker" | "theme" | "title" | "value">;
interface DxRangeSelector extends VueConstructor, AccessibleOptions {
    readonly instance?: RangeSelector;
}
declare const DxRangeSelector: DxRangeSelector;
declare const DxAggregation: any;
declare const DxAggregationInterval: any;
declare const DxArgumentFormat: any;
declare const DxBackground: any;
declare const DxBackgroundImage: any;
declare const DxBehavior: any;
declare const DxBorder: any;
declare const DxBreak: any;
declare const DxBreakStyle: any;
declare const DxChart: any;
declare const DxCommonSeriesSettings: any;
declare const DxCommonSeriesSettingsHoverStyle: any;
declare const DxCommonSeriesSettingsLabel: any;
declare const DxCommonSeriesSettingsSelectionStyle: any;
declare const DxConnector: any;
declare const DxDataPrepareSettings: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxHatching: any;
declare const DxHeight: any;
declare const DxHoverStyle: any;
declare const DxImage: any;
declare const DxIndent: any;
declare const DxLabel: any;
declare const DxLength: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxMarker: any;
declare const DxMarkerLabel: any;
declare const DxMaxRange: any;
declare const DxMinorTick: any;
declare const DxMinorTickInterval: any;
declare const DxMinRange: any;
declare const DxPoint: any;
declare const DxPointBorder: any;
declare const DxPointHoverStyle: any;
declare const DxPointImage: any;
declare const DxPointSelectionStyle: any;
declare const DxReduction: any;
declare const DxScale: any;
declare const DxScaleLabel: any;
declare const DxSelectionStyle: any;
declare const DxSeries: any;
declare const DxSeriesBorder: any;
declare const DxSeriesTemplate: any;
declare const DxShutter: any;
declare const DxSize: any;
declare const DxSliderHandle: any;
declare const DxSliderMarker: any;
declare const DxSubtitle: any;
declare const DxTick: any;
declare const DxTickInterval: any;
declare const DxTitle: any;
declare const DxUrl: any;
declare const DxValue: any;
declare const DxValueAxis: any;
declare const DxValueErrorBar: any;
declare const DxWidth: any;
export default DxRangeSelector;
export { DxRangeSelector, DxAggregation, DxAggregationInterval, DxArgumentFormat, DxBackground, DxBackgroundImage, DxBehavior, DxBorder, DxBreak, DxBreakStyle, DxChart, DxCommonSeriesSettings, DxCommonSeriesSettingsHoverStyle, DxCommonSeriesSettingsLabel, DxCommonSeriesSettingsSelectionStyle, DxConnector, DxDataPrepareSettings, DxExport, DxFont, DxFormat, DxHatching, DxHeight, DxHoverStyle, DxImage, DxIndent, DxLabel, DxLength, DxLoadingIndicator, DxMargin, DxMarker, DxMarkerLabel, DxMaxRange, DxMinorTick, DxMinorTickInterval, DxMinRange, DxPoint, DxPointBorder, DxPointHoverStyle, DxPointImage, DxPointSelectionStyle, DxReduction, DxScale, DxScaleLabel, DxSelectionStyle, DxSeries, DxSeriesBorder, DxSeriesTemplate, DxShutter, DxSize, DxSliderHandle, DxSliderMarker, DxSubtitle, DxTick, DxTickInterval, DxTitle, DxUrl, DxValue, DxValueAxis, DxValueErrorBar, DxWidth };
