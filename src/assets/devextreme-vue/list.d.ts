/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import List, { IOptions } from "devextreme/ui/list";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "allowItemDeleting" | "allowItemReordering" | "bounceEnabled" | "collapsibleGroups" | "dataSource" | "disabled" | "elementAttr" | "focusStateEnabled" | "grouped" | "groupTemplate" | "height" | "hint" | "hoverStateEnabled" | "indicateLoading" | "itemDeleteMode" | "itemHoldTimeout" | "items" | "itemTemplate" | "keyExpr" | "menuItems" | "menuMode" | "nextButtonText" | "noDataText" | "onContentReady" | "onDisposing" | "onGroupRendered" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemDeleted" | "onItemDeleting" | "onItemHold" | "onItemRendered" | "onItemReordered" | "onItemSwipe" | "onOptionChanged" | "onPageLoading" | "onPullRefresh" | "onScroll" | "onSelectAllValueChanged" | "onSelectionChanged" | "pageLoadingText" | "pageLoadMode" | "pulledDownText" | "pullingDownText" | "pullRefreshEnabled" | "refreshingText" | "repaintChangesOnly" | "rtlEnabled" | "scrollByContent" | "scrollByThumb" | "scrollingEnabled" | "searchEditorOptions" | "searchEnabled" | "searchExpr" | "searchMode" | "searchTimeout" | "searchValue" | "selectAllMode" | "selectedItemKeys" | "selectedItems" | "selectionMode" | "showScrollbar" | "showSelectionControls" | "tabIndex" | "useNativeScrolling" | "visible" | "width">;
interface DxList extends VueConstructor, AccessibleOptions {
    readonly instance?: List;
}
declare const DxList: DxList;
declare const DxItem: any;
declare const DxMenuItem: any;
declare const DxSearchEditorOptions: any;
export default DxList;
export { DxList, DxItem, DxMenuItem, DxSearchEditorOptions };
