/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var button_1 = require("devextreme/ui/button");
var component_1 = require("./core/component");
var DxButton = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        icon: String,
        onClick: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "text",
                "outlined",
                "contained"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        template: {},
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "back",
                "danger",
                "default",
                "normal",
                "success"
            ].indexOf(v) !== -1; }
        },
        useSubmitBehavior: Boolean,
        validationGroup: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = button_1.default;
    }
});
exports.DxButton = DxButton;
exports.default = DxButton;
