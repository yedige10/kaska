/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var slider_1 = require("devextreme/ui/slider");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxSlider = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        isValid: Boolean,
        keyStep: Number,
        label: Object,
        max: Number,
        min: Number,
        name: String,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onValueChanged: Function,
        readOnly: Boolean,
        rtlEnabled: Boolean,
        showRange: Boolean,
        step: Number,
        tabIndex: Number,
        tooltip: Object,
        validationError: Object,
        validationMessageMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "auto"
            ].indexOf(v) !== -1; }
        },
        value: Number,
        visible: Boolean,
        width: [Function, Number, String]
    },
    model: { prop: "value", event: "update:value" },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = slider_1.default;
        this.$_expectedChildren = {
            label: { isCollectionItem: false, optionName: "label" },
            tooltip: { isCollectionItem: false, optionName: "tooltip" }
        };
    }
});
exports.DxSlider = DxSlider;
var DxFormat = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        currency: String,
        formatter: Function,
        parser: Function,
        precision: Number,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "billions",
                "currency",
                "day",
                "decimal",
                "exponential",
                "fixedPoint",
                "largeNumber",
                "longDate",
                "longTime",
                "millions",
                "millisecond",
                "month",
                "monthAndDay",
                "monthAndYear",
                "percent",
                "quarter",
                "quarterAndYear",
                "shortDate",
                "shortTime",
                "thousands",
                "trillions",
                "year",
                "dayOfWeek",
                "hour",
                "longDateLongTime",
                "minute",
                "second",
                "shortDateShortTime"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxFormat = DxFormat;
DxFormat.$_optionName = "format";
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        format: [Object, Function, String],
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
DxLabel.$_expectedChildren = {
    format: { isCollectionItem: false, optionName: "format" }
};
var DxTooltip = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        enabled: Boolean,
        format: [Object, Function, String],
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "top"
            ].indexOf(v) !== -1; }
        },
        showMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "always",
                "onHover"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxTooltip = DxTooltip;
DxTooltip.$_optionName = "tooltip";
DxTooltip.$_expectedChildren = {
    format: { isCollectionItem: false, optionName: "format" }
};
exports.default = DxSlider;
