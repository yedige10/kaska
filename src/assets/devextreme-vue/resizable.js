/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var resizable_1 = require("devextreme/ui/resizable");
var component_1 = require("./core/component");
var DxResizable = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        elementAttr: Object,
        handles: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top",
                "all"
            ].indexOf(v) !== -1; }
        },
        height: [Function, Number, String],
        maxHeight: Number,
        maxWidth: Number,
        minHeight: Number,
        minWidth: Number,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onResize: Function,
        onResizeEnd: Function,
        onResizeStart: Function,
        rtlEnabled: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = resizable_1.default;
    }
});
exports.DxResizable = DxResizable;
exports.default = DxResizable;
