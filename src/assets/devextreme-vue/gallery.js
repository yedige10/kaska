/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var gallery_1 = require("devextreme/ui/gallery");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxGallery = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        animationDuration: Number,
        animationEnabled: Boolean,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        indicatorEnabled: Boolean,
        initialItemWidth: Number,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        loop: Boolean,
        onContentReady: Function,
        onDisposing: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        rtlEnabled: Boolean,
        selectedIndex: Number,
        selectedItem: Object,
        showIndicator: Boolean,
        showNavButtons: Boolean,
        slideshowDelay: Number,
        stretchImages: Boolean,
        swipeEnabled: Boolean,
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String],
        wrapAround: Boolean
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = gallery_1.default;
        this.$_expectedChildren = {
            item: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxGallery = DxGallery;
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        disabled: Boolean,
        html: String,
        imageAlt: String,
        imageSrc: String,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
exports.default = DxGallery;
