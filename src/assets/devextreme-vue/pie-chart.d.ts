/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import PieChart, { IOptions } from "devextreme/viz/pie_chart";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adaptiveLayout" | "animation" | "commonSeriesSettings" | "customizeLabel" | "customizePoint" | "dataSource" | "diameter" | "disabled" | "elementAttr" | "export" | "innerRadius" | "legend" | "loadingIndicator" | "margin" | "minDiameter" | "onDisposing" | "onDone" | "onDrawn" | "onExported" | "onExporting" | "onFileSaving" | "onIncidentOccurred" | "onInitialized" | "onLegendClick" | "onOptionChanged" | "onPointClick" | "onPointHoverChanged" | "onPointSelectionChanged" | "onTooltipHidden" | "onTooltipShown" | "palette" | "paletteExtensionMode" | "pathModified" | "pointSelectionMode" | "redrawOnResize" | "resolveLabelOverlapping" | "rtlEnabled" | "segmentsDirection" | "series" | "seriesTemplate" | "size" | "sizeGroup" | "startAngle" | "theme" | "title" | "tooltip" | "type">;
interface DxPieChart extends VueConstructor, AccessibleOptions {
    readonly instance?: PieChart;
}
declare const DxPieChart: DxPieChart;
declare const DxAdaptiveLayout: any;
declare const DxAnimation: any;
declare const DxArgumentFormat: any;
declare const DxBorder: any;
declare const DxCommonSeriesSettings: any;
declare const DxConnector: any;
declare const DxExport: any;
declare const DxFont: any;
declare const DxFormat: any;
declare const DxHatching: any;
declare const DxHoverStyle: any;
declare const DxLabel: any;
declare const DxLegend: any;
declare const DxLegendBorder: any;
declare const DxLoadingIndicator: any;
declare const DxMargin: any;
declare const DxSelectionStyle: any;
declare const DxSeries: any;
declare const DxSeriesBorder: any;
declare const DxSeriesTemplate: any;
declare const DxShadow: any;
declare const DxSize: any;
declare const DxSmallValuesGrouping: any;
declare const DxSubtitle: any;
declare const DxTitle: any;
declare const DxTooltip: any;
declare const DxTooltipBorder: any;
export default DxPieChart;
export { DxPieChart, DxAdaptiveLayout, DxAnimation, DxArgumentFormat, DxBorder, DxCommonSeriesSettings, DxConnector, DxExport, DxFont, DxFormat, DxHatching, DxHoverStyle, DxLabel, DxLegend, DxLegendBorder, DxLoadingIndicator, DxMargin, DxSelectionStyle, DxSeries, DxSeriesBorder, DxSeriesTemplate, DxShadow, DxSize, DxSmallValuesGrouping, DxSubtitle, DxTitle, DxTooltip, DxTooltipBorder };
