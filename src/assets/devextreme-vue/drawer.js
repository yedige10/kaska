/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var drawer_1 = require("devextreme/ui/drawer");
var component_1 = require("./core/component");
var DxDrawer = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        activeStateEnabled: Boolean,
        animationDuration: Number,
        animationEnabled: Boolean,
        closeOnOutsideClick: [Boolean, Function],
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        maxSize: Number,
        minSize: Number,
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        opened: Boolean,
        openedStateMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "overlap",
                "shrink",
                "push"
            ].indexOf(v) !== -1; }
        },
        position: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right",
                "top",
                "bottom",
                "before",
                "after"
            ].indexOf(v) !== -1; }
        },
        revealMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "slide",
                "expand"
            ].indexOf(v) !== -1; }
        },
        rtlEnabled: Boolean,
        shading: Boolean,
        target: {},
        template: {},
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = drawer_1.default;
    }
});
exports.DxDrawer = DxDrawer;
exports.default = DxDrawer;
