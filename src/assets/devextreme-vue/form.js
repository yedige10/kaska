/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var form_1 = require("devextreme/ui/form");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxForm = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        alignItemLabels: Boolean,
        alignItemLabelsInAllGroups: Boolean,
        colCount: {
            type: [Number, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "auto"
            ].indexOf(v) !== -1; }
        },
        colCountByScreen: Object,
        customizeItem: Function,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        formData: Object,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        items: Array,
        labelLocation: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        minColWidth: Number,
        onContentReady: Function,
        onDisposing: Function,
        onEditorEnterKey: Function,
        onFieldDataChanged: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        optionalMark: String,
        readOnly: Boolean,
        requiredMark: String,
        requiredMessage: String,
        rtlEnabled: Boolean,
        screenByWidth: Function,
        scrollingEnabled: Boolean,
        showColonAfterLabel: Boolean,
        showOptionalMark: Boolean,
        showRequiredMark: Boolean,
        showValidationSummary: Boolean,
        tabIndex: Number,
        validationGroup: String,
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = form_1.default;
        this.$_expectedChildren = {
            ButtonItem: { isCollectionItem: true, optionName: "items" },
            colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" },
            EmptyItem: { isCollectionItem: true, optionName: "items" },
            GroupItem: { isCollectionItem: true, optionName: "items" },
            item: { isCollectionItem: true, optionName: "items" },
            SimpleItem: { isCollectionItem: true, optionName: "items" },
            TabbedItem: { isCollectionItem: true, optionName: "items" }
        };
    }
});
exports.DxForm = DxForm;
var DxButtonItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        buttonOptions: Object,
        colSpan: Number,
        cssClass: String,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxButtonItem = DxButtonItem;
DxButtonItem.$_optionName = "items";
DxButtonItem.$_isCollectionItem = true;
DxButtonItem.$_predefinedProps = {
    itemType: "button"
};
DxButtonItem.$_expectedChildren = {
    buttonOptions: { isCollectionItem: false, optionName: "buttonOptions" }
};
var DxButtonOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        bindingOptions: Object,
        disabled: Boolean,
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        icon: String,
        onClick: [Function, String],
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        rtlEnabled: Boolean,
        stylingMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "text",
                "outlined",
                "contained"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        template: {},
        text: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "back",
                "danger",
                "default",
                "normal",
                "success"
            ].indexOf(v) !== -1; }
        },
        useSubmitBehavior: Boolean,
        validationGroup: String,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxButtonOptions = DxButtonOptions;
DxButtonOptions.$_optionName = "buttonOptions";
var DxColCountByScreen = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        lg: Number,
        md: Number,
        sm: Number,
        xs: Number
    }
});
exports.DxColCountByScreen = DxColCountByScreen;
DxColCountByScreen.$_optionName = "colCountByScreen";
var DxCompareRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCompareRule = DxCompareRule;
DxCompareRule.$_optionName = "validationRules";
DxCompareRule.$_isCollectionItem = true;
DxCompareRule.$_predefinedProps = {
    type: "compare"
};
var DxCustomRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxCustomRule = DxCustomRule;
DxCustomRule.$_optionName = "validationRules";
DxCustomRule.$_isCollectionItem = true;
DxCustomRule.$_predefinedProps = {
    type: "custom"
};
var DxEmailRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxEmailRule = DxEmailRule;
DxEmailRule.$_optionName = "validationRules";
DxEmailRule.$_isCollectionItem = true;
DxEmailRule.$_predefinedProps = {
    type: "email"
};
var DxEmptyItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxEmptyItem = DxEmptyItem;
DxEmptyItem.$_optionName = "items";
DxEmptyItem.$_isCollectionItem = true;
DxEmptyItem.$_predefinedProps = {
    itemType: "empty"
};
var DxGroupItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        caption: String,
        colCount: Number,
        colCountByScreen: Object,
        colSpan: Number,
        cssClass: String,
        items: Array,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        template: {},
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxGroupItem = DxGroupItem;
DxGroupItem.$_optionName = "items";
DxGroupItem.$_isCollectionItem = true;
DxGroupItem.$_predefinedProps = {
    itemType: "group"
};
DxGroupItem.$_expectedChildren = {
    colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" }
};
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        badge: String,
        buttonOptions: Object,
        caption: String,
        colCount: Number,
        colCountByScreen: Object,
        colSpan: Number,
        cssClass: String,
        dataField: String,
        disabled: Boolean,
        editorOptions: Object,
        editorType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxCalendar",
                "dxCheckBox",
                "dxColorBox",
                "dxDateBox",
                "dxDropDownBox",
                "dxLookup",
                "dxNumberBox",
                "dxRadioGroup",
                "dxRangeSlider",
                "dxSelectBox",
                "dxSlider",
                "dxSwitch",
                "dxTagBox",
                "dxTextArea",
                "dxTextBox"
            ].indexOf(v) !== -1; }
        },
        helpText: String,
        horizontalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        html: String,
        icon: String,
        isRequired: Boolean,
        items: Array,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        name: String,
        tabPanelOptions: Object,
        tabs: Array,
        tabTemplate: {},
        template: {},
        text: String,
        title: String,
        validationRules: Array,
        verticalAlignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        },
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxLabel = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignment: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        location: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        showColon: Boolean,
        text: String,
        visible: Boolean
    }
});
exports.DxLabel = DxLabel;
DxLabel.$_optionName = "label";
var DxNumericRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxNumericRule = DxNumericRule;
DxNumericRule.$_optionName = "validationRules";
DxNumericRule.$_isCollectionItem = true;
DxNumericRule.$_predefinedProps = {
    type: "numeric"
};
var DxPatternRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        message: String,
        pattern: {},
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxPatternRule = DxPatternRule;
DxPatternRule.$_optionName = "validationRules";
DxPatternRule.$_isCollectionItem = true;
DxPatternRule.$_predefinedProps = {
    type: "pattern"
};
var DxRangeRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        reevaluate: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRangeRule = DxRangeRule;
DxRangeRule.$_optionName = "validationRules";
DxRangeRule.$_isCollectionItem = true;
DxRangeRule.$_predefinedProps = {
    type: "range"
};
var DxRequiredRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        message: String,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxRequiredRule = DxRequiredRule;
DxRequiredRule.$_optionName = "validationRules";
DxRequiredRule.$_isCollectionItem = true;
DxRequiredRule.$_predefinedProps = {
    type: "required"
};
var DxSimpleItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        dataField: String,
        editorOptions: Object,
        editorType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "dxAutocomplete",
                "dxCalendar",
                "dxCheckBox",
                "dxColorBox",
                "dxDateBox",
                "dxDropDownBox",
                "dxLookup",
                "dxNumberBox",
                "dxRadioGroup",
                "dxRangeSlider",
                "dxSelectBox",
                "dxSlider",
                "dxSwitch",
                "dxTagBox",
                "dxTextArea",
                "dxTextBox"
            ].indexOf(v) !== -1; }
        },
        helpText: String,
        isRequired: Boolean,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        label: Object,
        name: String,
        template: {},
        validationRules: Array,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxSimpleItem = DxSimpleItem;
DxSimpleItem.$_optionName = "items";
DxSimpleItem.$_isCollectionItem = true;
DxSimpleItem.$_predefinedProps = {
    itemType: "simple"
};
DxSimpleItem.$_expectedChildren = {
    CompareRule: { isCollectionItem: true, optionName: "validationRules" },
    CustomRule: { isCollectionItem: true, optionName: "validationRules" },
    EmailRule: { isCollectionItem: true, optionName: "validationRules" },
    label: { isCollectionItem: false, optionName: "label" },
    NumericRule: { isCollectionItem: true, optionName: "validationRules" },
    PatternRule: { isCollectionItem: true, optionName: "validationRules" },
    RangeRule: { isCollectionItem: true, optionName: "validationRules" },
    RequiredRule: { isCollectionItem: true, optionName: "validationRules" },
    StringLengthRule: { isCollectionItem: true, optionName: "validationRules" },
    validationRule: { isCollectionItem: true, optionName: "validationRules" }
};
var DxStringLengthRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        ignoreEmptyValue: Boolean,
        max: Number,
        message: String,
        min: Number,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxStringLengthRule = DxStringLengthRule;
DxStringLengthRule.$_optionName = "validationRules";
DxStringLengthRule.$_isCollectionItem = true;
DxStringLengthRule.$_predefinedProps = {
    type: "stringLength"
};
var DxTab = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        alignItemLabels: Boolean,
        badge: String,
        colCount: Number,
        colCountByScreen: Object,
        disabled: Boolean,
        icon: String,
        items: Array,
        tabTemplate: {},
        template: {},
        title: String
    }
});
exports.DxTab = DxTab;
DxTab.$_optionName = "tabs";
DxTab.$_isCollectionItem = true;
DxTab.$_expectedChildren = {
    colCountByScreen: { isCollectionItem: false, optionName: "colCountByScreen" }
};
var DxTabbedItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        colSpan: Number,
        cssClass: String,
        itemType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "empty",
                "group",
                "simple",
                "tabbed",
                "button"
            ].indexOf(v) !== -1; }
        },
        name: String,
        tabPanelOptions: Object,
        tabs: Array,
        visible: Boolean,
        visibleIndex: Number
    }
});
exports.DxTabbedItem = DxTabbedItem;
DxTabbedItem.$_optionName = "items";
DxTabbedItem.$_isCollectionItem = true;
DxTabbedItem.$_predefinedProps = {
    itemType: "tabbed"
};
DxTabbedItem.$_expectedChildren = {
    tab: { isCollectionItem: true, optionName: "tabs" },
    tabPanelOptions: { isCollectionItem: false, optionName: "tabPanelOptions" }
};
var DxTabPanelOptions = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animationEnabled: Boolean,
        bindingOptions: Object,
        dataSource: [Array, Object, String],
        deferRendering: Boolean,
        disabled: Boolean,
        elementAttr: Object,
        focusedElement: {},
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        itemHoldTimeout: Number,
        items: Array,
        itemTemplate: {},
        itemTitleTemplate: {},
        keyExpr: [Function, String],
        loop: Boolean,
        loopItemFocus: Boolean,
        noDataText: String,
        onContentReady: Function,
        onDisposing: Function,
        onFocusIn: Function,
        onFocusOut: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemDeleted: Function,
        onItemDeleting: Function,
        onItemHold: Function,
        onItemRendered: Function,
        onItemReordered: Function,
        onOptionChanged: Function,
        onSelectionChanged: Function,
        onTitleClick: [Function, String],
        onTitleHold: Function,
        onTitleRendered: Function,
        repaintChangesOnly: Boolean,
        rtlEnabled: Boolean,
        scrollByContent: Boolean,
        scrollingEnabled: Boolean,
        selectedIndex: Number,
        selectedItem: Object,
        selectedItemKeys: Array,
        selectedItems: Array,
        selectionByClick: Boolean,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "multiple",
                "single",
                "all",
                "none"
            ].indexOf(v) !== -1; }
        },
        selectionRequired: Boolean,
        selectOnFocus: Boolean,
        showNavButtons: Boolean,
        swipeEnabled: Boolean,
        tabIndex: Number,
        visible: Boolean,
        width: [Function, Number, String]
    }
});
exports.DxTabPanelOptions = DxTabPanelOptions;
DxTabPanelOptions.$_optionName = "tabPanelOptions";
DxTabPanelOptions.$_expectedChildren = {
    item: { isCollectionItem: true, optionName: "items" },
    tabPanelOptionsItem: { isCollectionItem: true, optionName: "items" }
};
var DxTabPanelOptionsItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        badge: String,
        disabled: Boolean,
        html: String,
        icon: String,
        tabTemplate: {},
        template: {},
        text: String,
        title: String,
        visible: Boolean
    }
});
exports.DxTabPanelOptionsItem = DxTabPanelOptionsItem;
DxTabPanelOptionsItem.$_optionName = "items";
DxTabPanelOptionsItem.$_isCollectionItem = true;
var DxValidationRule = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        comparisonTarget: Function,
        comparisonType: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "!=",
                "!==",
                "<",
                "<=",
                "==",
                "===",
                ">",
                ">="
            ].indexOf(v) !== -1; }
        },
        ignoreEmptyValue: Boolean,
        max: {},
        message: String,
        min: {},
        pattern: {},
        reevaluate: Boolean,
        trim: Boolean,
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "required",
                "numeric",
                "range",
                "stringLength",
                "custom",
                "compare",
                "pattern",
                "email"
            ].indexOf(v) !== -1; }
        },
        validationCallback: Function
    }
});
exports.DxValidationRule = DxValidationRule;
DxValidationRule.$_optionName = "validationRules";
DxValidationRule.$_isCollectionItem = true;
DxValidationRule.$_predefinedProps = {
    type: "required"
};
exports.default = DxForm;
