/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Slider, { IOptions } from "devextreme/ui/slider";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "disabled" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "isValid" | "keyStep" | "label" | "max" | "min" | "name" | "onContentReady" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValueChanged" | "readOnly" | "rtlEnabled" | "showRange" | "step" | "tabIndex" | "tooltip" | "validationError" | "validationMessageMode" | "value" | "visible" | "width">;
interface DxSlider extends VueConstructor, AccessibleOptions {
    readonly instance?: Slider;
}
declare const DxSlider: DxSlider;
declare const DxFormat: any;
declare const DxLabel: any;
declare const DxTooltip: any;
export default DxSlider;
export { DxSlider, DxFormat, DxLabel, DxTooltip };
