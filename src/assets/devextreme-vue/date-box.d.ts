/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import DateBox, { IOptions } from "devextreme/ui/date_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "acceptCustomValue" | "accessKey" | "activeStateEnabled" | "adaptivityEnabled" | "applyButtonText" | "applyValueMode" | "calendarOptions" | "cancelButtonText" | "dateOutOfRangeMessage" | "dateSerializationFormat" | "deferRendering" | "disabled" | "disabledDates" | "displayFormat" | "dropDownButtonTemplate" | "elementAttr" | "focusStateEnabled" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "interval" | "invalidDateMessage" | "isValid" | "max" | "maxLength" | "min" | "name" | "onChange" | "onClosed" | "onContentReady" | "onCopy" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOpened" | "onOptionChanged" | "onPaste" | "onValueChanged" | "opened" | "openOnFieldClick" | "pickerType" | "placeholder" | "readOnly" | "rtlEnabled" | "showAnalogClock" | "showClearButton" | "showDropDownButton" | "spellcheck" | "stylingMode" | "tabIndex" | "text" | "type" | "useMaskBehavior" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "visible" | "width">;
interface DxDateBox extends VueConstructor, AccessibleOptions {
    readonly instance?: DateBox;
}
declare const DxDateBox: DxDateBox;
declare const DxCalendarOptions: any;
declare const DxDisplayFormat: any;
export default DxDateBox;
export { DxDateBox, DxCalendarOptions, DxDisplayFormat };
