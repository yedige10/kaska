/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var context_menu_1 = require("devextreme/ui/context_menu");
var component_1 = require("./core/component");
var configuration_component_1 = require("./core/configuration-component");
var DxContextMenu = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        accessKey: String,
        activeStateEnabled: Boolean,
        animation: Object,
        closeOnOutsideClick: [Boolean, Function],
        cssClass: String,
        dataSource: [Array, Object, String],
        disabled: Boolean,
        disabledExpr: [Function, String],
        displayExpr: [Function, String],
        elementAttr: Object,
        focusStateEnabled: Boolean,
        height: [Function, Number, String],
        hint: String,
        hoverStateEnabled: Boolean,
        items: Array,
        itemsExpr: [Function, String],
        itemTemplate: {},
        onContentReady: Function,
        onDisposing: Function,
        onHidden: Function,
        onHiding: Function,
        onInitialized: Function,
        onItemClick: [Function, String],
        onItemContextMenu: Function,
        onItemRendered: Function,
        onOptionChanged: Function,
        onPositioning: Function,
        onSelectionChanged: Function,
        onShowing: Function,
        onShown: Function,
        position: Object,
        rtlEnabled: Boolean,
        selectByClick: Boolean,
        selectedExpr: [Function, String],
        selectedItem: Object,
        selectionMode: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "none",
                "single"
            ].indexOf(v) !== -1; }
        },
        showEvent: [Object, String],
        showSubmenuMode: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        },
        submenuDirection: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "auto",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        tabIndex: Number,
        target: {},
        visible: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = context_menu_1.default;
        this.$_expectedChildren = {
            animation: { isCollectionItem: false, optionName: "animation" },
            item: { isCollectionItem: true, optionName: "items" },
            position: { isCollectionItem: false, optionName: "position" },
            showEvent: { isCollectionItem: false, optionName: "showEvent" },
            showSubmenuMode: { isCollectionItem: false, optionName: "showSubmenuMode" }
        };
    }
});
exports.DxContextMenu = DxContextMenu;
var DxAnimation = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Object,
        show: Object
    }
});
exports.DxAnimation = DxAnimation;
DxAnimation.$_optionName = "animation";
DxAnimation.$_expectedChildren = {
    hide: { isCollectionItem: false, optionName: "hide" },
    show: { isCollectionItem: false, optionName: "show" }
};
var DxAt = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxAt = DxAt;
DxAt.$_optionName = "at";
var DxBoundaryOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxBoundaryOffset = DxBoundaryOffset;
DxBoundaryOffset.$_optionName = "boundaryOffset";
var DxCollision = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "flip",
                "flipfit",
                "none"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxCollision = DxCollision;
DxCollision.$_optionName = "collision";
var DxDelay = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        hide: Number,
        show: Number
    }
});
exports.DxDelay = DxDelay;
DxDelay.$_optionName = "delay";
var DxHide = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxHide = DxHide;
DxHide.$_optionName = "hide";
var DxItem = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        beginGroup: Boolean,
        closeMenuOnClick: Boolean,
        disabled: Boolean,
        html: String,
        icon: String,
        items: Array,
        selectable: Boolean,
        selected: Boolean,
        template: {},
        text: String,
        visible: Boolean
    }
});
exports.DxItem = DxItem;
DxItem.$_optionName = "items";
DxItem.$_isCollectionItem = true;
var DxMy = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "center",
                "left",
                "right"
            ].indexOf(v) !== -1; }
        },
        y: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "top"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxMy = DxMy;
DxMy.$_optionName = "my";
var DxOffset = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        x: Number,
        y: Number
    }
});
exports.DxOffset = DxOffset;
DxOffset.$_optionName = "offset";
var DxPosition = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        at: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        boundary: {},
        boundaryOffset: [Object, String],
        collision: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "fit",
                "fit flip",
                "fit flipfit",
                "fit none",
                "flip",
                "flip fit",
                "flip none",
                "flipfit",
                "flipfit fit",
                "flipfit none",
                "none",
                "none fit",
                "none flip",
                "none flipfit"
            ].indexOf(v) !== -1; }
        },
        my: {
            type: [Object, String],
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "center",
                "left",
                "left bottom",
                "left top",
                "right",
                "right bottom",
                "right top",
                "top"
            ].indexOf(v) !== -1; }
        },
        of: {},
        offset: [Object, String]
    }
});
exports.DxPosition = DxPosition;
DxPosition.$_optionName = "position";
DxPosition.$_expectedChildren = {
    at: { isCollectionItem: false, optionName: "at" },
    boundaryOffset: { isCollectionItem: false, optionName: "boundaryOffset" },
    collision: { isCollectionItem: false, optionName: "collision" },
    my: { isCollectionItem: false, optionName: "my" },
    offset: { isCollectionItem: false, optionName: "offset" }
};
var DxShow = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        complete: Function,
        delay: Number,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "bottom",
                "left",
                "right",
                "top"
            ].indexOf(v) !== -1; }
        },
        duration: Number,
        easing: String,
        from: [Number, Object, String],
        staggerDelay: Number,
        start: Function,
        to: [Number, Object, String],
        type: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "css",
                "fade",
                "fadeIn",
                "fadeOut",
                "pop",
                "slide",
                "slideIn",
                "slideOut"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShow = DxShow;
DxShow.$_optionName = "show";
var DxShowEvent = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        delay: Number,
        name: String
    }
});
exports.DxShowEvent = DxShowEvent;
DxShowEvent.$_optionName = "showEvent";
var DxShowSubmenuMode = Vue.extend({
    extends: configuration_component_1.DxConfiguration,
    props: {
        delay: [Number, Object],
        name: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "onClick",
                "onHover"
            ].indexOf(v) !== -1; }
        }
    }
});
exports.DxShowSubmenuMode = DxShowSubmenuMode;
DxShowSubmenuMode.$_optionName = "showSubmenuMode";
DxShowSubmenuMode.$_expectedChildren = {
    delay: { isCollectionItem: false, optionName: "delay" }
};
exports.default = DxContextMenu;
