/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VueType = require("vue");
var Vue = VueType.default || VueType;
var scroll_view_1 = require("devextreme/ui/scroll_view");
var component_1 = require("./core/component");
var DxScrollView = Vue.extend({
    extends: component_1.DxComponent,
    props: {
        bounceEnabled: Boolean,
        direction: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "both",
                "horizontal",
                "vertical"
            ].indexOf(v) !== -1; }
        },
        disabled: Boolean,
        elementAttr: Object,
        height: [Function, Number, String],
        onDisposing: Function,
        onInitialized: Function,
        onOptionChanged: Function,
        onPullDown: Function,
        onReachBottom: Function,
        onScroll: Function,
        onUpdated: Function,
        pulledDownText: String,
        pullingDownText: String,
        reachBottomText: String,
        refreshingText: String,
        rtlEnabled: Boolean,
        scrollByContent: Boolean,
        scrollByThumb: Boolean,
        showScrollbar: {
            type: String,
            validator: function (v) { return typeof (v) !== "string" || [
                "onScroll",
                "onHover",
                "always",
                "never"
            ].indexOf(v) !== -1; }
        },
        useNative: Boolean,
        width: [Function, Number, String]
    },
    computed: {
        instance: function () {
            return this.$_instance;
        }
    },
    beforeCreate: function () {
        this.$_WidgetClass = scroll_view_1.default;
    }
});
exports.DxScrollView = DxScrollView;
exports.default = DxScrollView;
