/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import TagBox, { IOptions } from "devextreme/ui/tag_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "acceptCustomValue" | "accessKey" | "activeStateEnabled" | "applyValueMode" | "dataSource" | "deferRendering" | "disabled" | "displayExpr" | "dropDownButtonTemplate" | "elementAttr" | "fieldTemplate" | "focusStateEnabled" | "grouped" | "groupTemplate" | "height" | "hideSelectedItems" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "items" | "itemTemplate" | "maxDisplayedTags" | "minSearchLength" | "multiline" | "name" | "noDataText" | "onChange" | "onClosed" | "onContentReady" | "onCustomItemCreating" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onItemClick" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onMultiTagPreparing" | "onOpened" | "onOptionChanged" | "onSelectAllValueChanged" | "onSelectionChanged" | "onValueChanged" | "opened" | "openOnFieldClick" | "placeholder" | "readOnly" | "rtlEnabled" | "searchEnabled" | "searchExpr" | "searchMode" | "searchTimeout" | "selectAllMode" | "selectedItems" | "showClearButton" | "showDataBeforeSearch" | "showDropDownButton" | "showMultiTagOnly" | "showSelectionControls" | "stylingMode" | "tabIndex" | "tagTemplate" | "text" | "validationError" | "validationMessageMode" | "value" | "valueExpr" | "visible" | "width">;
interface DxTagBox extends VueConstructor, AccessibleOptions {
    readonly instance?: TagBox;
}
declare const DxTagBox: DxTagBox;
declare const DxItem: any;
export default DxTagBox;
export { DxTagBox, DxItem };
