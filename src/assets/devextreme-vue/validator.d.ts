/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Validator, { IOptions } from "devextreme/ui/validator";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "adapter" | "elementAttr" | "height" | "name" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onValidated" | "validationGroup" | "validationRules" | "width">;
interface DxValidator extends VueConstructor, AccessibleOptions {
    readonly instance?: Validator;
}
declare const DxValidator: DxValidator;
declare const DxAdapter: any;
declare const DxCompareRule: any;
declare const DxCustomRule: any;
declare const DxEmailRule: any;
declare const DxNumericRule: any;
declare const DxPatternRule: any;
declare const DxRangeRule: any;
declare const DxRequiredRule: any;
declare const DxStringLengthRule: any;
declare const DxValidationRule: any;
export default DxValidator;
export { DxValidator, DxAdapter, DxCompareRule, DxCustomRule, DxEmailRule, DxNumericRule, DxPatternRule, DxRangeRule, DxRequiredRule, DxStringLengthRule, DxValidationRule };
