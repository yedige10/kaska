/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import SelectBox, { IOptions } from "devextreme/ui/select_box";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "acceptCustomValue" | "accessKey" | "activeStateEnabled" | "dataSource" | "deferRendering" | "disabled" | "displayExpr" | "displayValue" | "dropDownButtonTemplate" | "elementAttr" | "fieldTemplate" | "focusStateEnabled" | "grouped" | "groupTemplate" | "height" | "hint" | "hoverStateEnabled" | "inputAttr" | "isValid" | "items" | "itemTemplate" | "maxLength" | "minSearchLength" | "name" | "noDataText" | "onChange" | "onClosed" | "onContentReady" | "onCopy" | "onCustomItemCreating" | "onCut" | "onDisposing" | "onEnterKey" | "onFocusIn" | "onFocusOut" | "onInitialized" | "onInput" | "onItemClick" | "onKeyDown" | "onKeyPress" | "onKeyUp" | "onOpened" | "onOptionChanged" | "onPaste" | "onSelectionChanged" | "onValueChanged" | "opened" | "openOnFieldClick" | "placeholder" | "readOnly" | "rtlEnabled" | "searchEnabled" | "searchExpr" | "searchMode" | "searchTimeout" | "selectedItem" | "showClearButton" | "showDataBeforeSearch" | "showDropDownButton" | "showSelectionControls" | "spellcheck" | "stylingMode" | "tabIndex" | "text" | "validationError" | "validationMessageMode" | "value" | "valueChangeEvent" | "valueExpr" | "visible" | "width">;
interface DxSelectBox extends VueConstructor, AccessibleOptions {
    readonly instance?: SelectBox;
}
declare const DxSelectBox: DxSelectBox;
declare const DxItem: any;
export default DxSelectBox;
export { DxSelectBox, DxItem };
