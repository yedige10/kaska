/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Form, { IOptions } from "devextreme/ui/form";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "accessKey" | "activeStateEnabled" | "alignItemLabels" | "alignItemLabelsInAllGroups" | "colCount" | "colCountByScreen" | "customizeItem" | "disabled" | "elementAttr" | "focusStateEnabled" | "formData" | "height" | "hint" | "hoverStateEnabled" | "items" | "labelLocation" | "minColWidth" | "onContentReady" | "onDisposing" | "onEditorEnterKey" | "onFieldDataChanged" | "onInitialized" | "onOptionChanged" | "optionalMark" | "readOnly" | "requiredMark" | "requiredMessage" | "rtlEnabled" | "screenByWidth" | "scrollingEnabled" | "showColonAfterLabel" | "showOptionalMark" | "showRequiredMark" | "showValidationSummary" | "tabIndex" | "validationGroup" | "visible" | "width">;
interface DxForm extends VueConstructor, AccessibleOptions {
    readonly instance?: Form;
}
declare const DxForm: DxForm;
declare const DxButtonItem: any;
declare const DxButtonOptions: any;
declare const DxColCountByScreen: any;
declare const DxCompareRule: any;
declare const DxCustomRule: any;
declare const DxEmailRule: any;
declare const DxEmptyItem: any;
declare const DxGroupItem: any;
declare const DxItem: any;
declare const DxLabel: any;
declare const DxNumericRule: any;
declare const DxPatternRule: any;
declare const DxRangeRule: any;
declare const DxRequiredRule: any;
declare const DxSimpleItem: any;
declare const DxStringLengthRule: any;
declare const DxTab: any;
declare const DxTabbedItem: any;
declare const DxTabPanelOptions: any;
declare const DxTabPanelOptionsItem: any;
declare const DxValidationRule: any;
export default DxForm;
export { DxForm, DxButtonItem, DxButtonOptions, DxColCountByScreen, DxCompareRule, DxCustomRule, DxEmailRule, DxEmptyItem, DxGroupItem, DxItem, DxLabel, DxNumericRule, DxPatternRule, DxRangeRule, DxRequiredRule, DxSimpleItem, DxStringLengthRule, DxTab, DxTabbedItem, DxTabPanelOptions, DxTabPanelOptionsItem, DxValidationRule };
