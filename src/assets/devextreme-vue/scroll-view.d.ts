/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import ScrollView, { IOptions } from "devextreme/ui/scroll_view";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "bounceEnabled" | "direction" | "disabled" | "elementAttr" | "height" | "onDisposing" | "onInitialized" | "onOptionChanged" | "onPullDown" | "onReachBottom" | "onScroll" | "onUpdated" | "pulledDownText" | "pullingDownText" | "reachBottomText" | "refreshingText" | "rtlEnabled" | "scrollByContent" | "scrollByThumb" | "showScrollbar" | "useNative" | "width">;
interface DxScrollView extends VueConstructor, AccessibleOptions {
    readonly instance?: ScrollView;
}
declare const DxScrollView: DxScrollView;
export default DxScrollView;
export { DxScrollView };
