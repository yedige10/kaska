/*!
 * devextreme-vue
 * Version: 18.2.6
 * Build date: Wed Feb 06 2019
 *
 * Copyright (c) 2012 - 2019 Developer Express Inc. ALL RIGHTS RESERVED
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file in the root of the project for details.
 *
 * https://github.com/DevExpress/devextreme-vue
 */

import Toolbar, { IOptions } from "devextreme/ui/toolbar";
import { VueConstructor } from "vue";
declare type AccessibleOptions = Pick<IOptions, "dataSource" | "disabled" | "elementAttr" | "height" | "hint" | "hoverStateEnabled" | "itemHoldTimeout" | "items" | "itemTemplate" | "menuItemTemplate" | "noDataText" | "onContentReady" | "onDisposing" | "onInitialized" | "onItemClick" | "onItemContextMenu" | "onItemHold" | "onItemRendered" | "onOptionChanged" | "renderAs" | "rtlEnabled" | "visible" | "width">;
interface DxToolbar extends VueConstructor, AccessibleOptions {
    readonly instance?: Toolbar;
}
declare const DxToolbar: DxToolbar;
declare const DxItem: any;
export default DxToolbar;
export { DxToolbar, DxItem };
