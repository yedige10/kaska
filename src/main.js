// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSlideoutPanel from 'vue2-slideout-panel';
import BootstrapVue from 'bootstrap-vue'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import VModal from 'vue-js-modal'
import VueSwal from 'vue-swal'
import VueDragDrop from 'vue-drag-drop';
import VueResource from 'vue-resource';
require('jquery/dist/jquery');
require('popper.js/dist/umd/popper');
require('bootstrap/dist/js/bootstrap');
require('moment');
export const api="http://192.168.8.107:8000";
//uses
Vue.use(VueSlideoutPanel);
Vue.use(BootstrapVue)
Vue.use(PerfectScrollbar)
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.use(VueSwal)
Vue.use(VueDragDrop);
Vue.use(VueResource)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
